export default class ApiModel<T> {
  public id!: number;
  public createdBy?: string;
  public createdDate?: number;
  public lastModifiedBy?: string;
  public lastModifiedDate?: number;
  public isDeleted?: boolean;

  constructor(urlHost: string, urlResource: string, data: any) {
    if (data) {
      Object.assign(this, data);
    }
  }

  // $get() {
  // }

  // $delete() {
  // }

  // $post(): {
  // }

  // $put() {
  // }
}
