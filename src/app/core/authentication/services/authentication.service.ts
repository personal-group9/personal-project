import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private loggedIn: BehaviorSubject<boolean>;

  constructor(
    public angularFireAuth: AngularFireAuth,
    public router: Router,
    private toastrService: ToastrService,
    public translateService: TranslateService
  ) {
    this.loggedIn = new BehaviorSubject<boolean>(false);
    if (localStorage.getItem('user')){
      this.loggedIn.next(true);
    }
    this.initUserAuthentification();
  }

  private initUserAuthentification(): void {
    this.angularFireAuth.authState.subscribe(user => {
      if (user) {
        this.loggedIn.next(true);
        localStorage.setItem('user', JSON.stringify(user));
      } else {
        this.loggedIn.next(false);
        localStorage.setItem('user', '');
      }
    });
  }

  get isLoggedIn(): boolean {
    return this.loggedIn.getValue();
  }

  isLoggedInObservable(): Observable<boolean> {
    return this.loggedIn.asObservable();
  }

  login(email: string, password: string): void {
    this.angularFireAuth.signInWithEmailAndPassword(email, password).then((user) => {
      this.translateService.get('MODULES.COMMON.LOGIN.SUCCESS_LOGIN').subscribe((message: string) => {
        this.toastrService.success(message);
      });
      localStorage.setItem('user', JSON.stringify(user));
      this.router.navigate(['/home']);
    }).catch(() => {
      this.translateService.get('MODULES.COMMON.LOGIN.ERROR_LOGIN').subscribe((message: string) => this.toastrService.error(message));
    });
  }

  logout(): void {
    this.angularFireAuth.signOut().then(() => {
      localStorage.removeItem('user');
      this.translateService.get('LAYOUT.NAVBAR.SUCCESS_LOGOUT').subscribe((message: string) => this.toastrService.success(message));
      this.router.navigate(['/home']);
    });
  }

}
