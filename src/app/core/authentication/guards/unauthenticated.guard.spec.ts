import { TestBed } from '@angular/core/testing';
import { UnauthenticatedGuard } from './unauthenticated-guard.service';
import { RouterTestingModule } from '@angular/router/testing';
import { CoreModule } from '@core/core.module';
import { SharedModule } from '@shared/shared.module';

describe('UnauthenticatedGuard', () => {
  let guard: UnauthenticatedGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CoreModule, SharedModule, RouterTestingModule]
    });
    guard = TestBed.inject(UnauthenticatedGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
