import { NgModule } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularFireModule } from '@angular/fire';
import { environment } from '@environments/environment';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { ErrorsModule } from '@core/errors/errors.module';
import { CookieService } from 'ngx-cookie-service';
import { Title } from '@angular/platform-browser';

export function httpTranslateLoader(http: HttpClient): any {
  return new TranslateHttpLoader(http);
}

@NgModule({
  imports: [
    BrowserAnimationsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: httpTranslateLoader,
        deps: [HttpClient]
      }
    }),
    ErrorsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    // AngularFirestoreModule, // Only required for database features
    // AngularFireStorageModule // Only required for storage features
  ],
  providers: [
    CookieService,
    Title
  ]
})
export class CoreModule {
}
