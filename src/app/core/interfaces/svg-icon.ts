export interface SvgIcon {
  name: string;
  url: string;
}
