import { SvgIcon } from '@core/interfaces/svg-icon';

export interface Language {
  name: string;
  value: string;
  icon?: SvgIcon;
}
