export interface Theme {
  name: string;
  value: string;
  icon?: string;
  iconColor?: string;
}
