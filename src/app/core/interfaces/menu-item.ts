export interface MenuItem {
  title: string;
  disabled?: boolean;
  icon?: string;
  route: string;
  children?: MenuItem[];
}
