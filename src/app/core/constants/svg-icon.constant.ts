import { SvgIcon } from '@core/interfaces/svg-icon';

/**
 * Define all svg icons and foreach in app.component.ts
 */
export const SVG_ICONS_LIST: SvgIcon[] = [
  {name: 'english-flag', url: '../assets/icons/english-flag.svg'},
  {name: 'french-flag', url: '../assets/icons/french-flag.svg'},
  {name: 'angular', url: '../assets/icons/angular.svg'},
  {name: 'gitlab', url: '../assets/icons/gitlab.svg'},
  {name: 'sonarcloud', url: '../assets/icons/sonarcloud.svg'},
  {name: 'jfrog', url: '../assets/icons/jfrog.svg'},
  {name: 'firebase', url: '../assets/icons/firebase.svg'},
  {name: 'heroku', url: '../assets/icons/heroku.svg'},
  {name: 'bootstrap', url: '../assets/icons/bootstrap.svg'},
  {name: 'angular-material', url: '../assets/icons/angular-material.svg'},
];
