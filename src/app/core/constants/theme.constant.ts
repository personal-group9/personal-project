import { Theme } from '@core/interfaces/theme';

/**
 * All themes define in @app/styles/themes.scss
 */
export const THEMES_LIST: Theme[] = [
  {name: 'LAYOUT.NAVBAR.THEMES.CLASSIC_THEME', value: 'classic-theme', icon: 'circle', iconColor: '#3f51b5'},
  {name: 'LAYOUT.NAVBAR.THEMES.LIGHT_THEME', value: 'light-theme', icon: 'circle', iconColor: '#eeeeee'},
  {name: 'LAYOUT.NAVBAR.THEMES.DARK_THEME', value: 'dark-theme', icon: 'circle', iconColor: '#616161'}
];

/**
 * Default theme can be modify here
 */
export const DEFAULT_THEME: Theme = THEMES_LIST[0];
