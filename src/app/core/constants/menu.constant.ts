import { MenuItem } from '@core/interfaces/menu-item';

/**
 * In the first level of menu, avoid or not multi opened panels in sidenav.component.ts
 */
export const MULTI_EXPANSION_PANELS_MENU = false;

/**
 * How much padding you want to move in the left for each new level menu. in pixel.
 */
export const PADDING_LEFT_LEVEL_PX_MENU = 16;

/**
 * Opening menu behavior
 */
export const FIXED_MENU = true;

export const WIDTH_PX_MENU = 260;

export const TUTORIAL_MENU: MenuItem[] = [
  {
    title: 'MODULES.ARCHITECTURES.TITLE',
    icon: 'architecture',
    route: '/tutorial/architectures',
    children: [
      {
        title: 'MODULES.ARCHITECTURES.SOFTWARE_ARCHITECTURE.TITLE',
        icon: 'subdirectory_arrow_right',
        route: '/tutorial/architectures/software-architecture'
      },
      {
        title: 'MODULES.ARCHITECTURES.FOLDERS_ORGANIZATION.TITLE',
        icon: 'subdirectory_arrow_right',
        route: '/tutorial/architectures/folders-organization'
      },
      {
        title: 'MODULES.ARCHITECTURES.PIPELINE_DIAGRAM.TITLE',
        icon: 'subdirectory_arrow_right',
        route: '/tutorial/architectures/pipeline-diagram'
      }
    ]
  },
  {
    title: 'MODULES.CODE_ANALYSIS.TITLE',
    icon: 'graphic_eq',
    route: '/tutorial/code-analysis',
    children: [
      {
        title: 'MODULES.CODE_ANALYSIS.CODE_QUALITY.TITLE',
        icon: 'subdirectory_arrow_right',
        route: '/tutorial/code-analysis/code-quality'
      },
      {
        title: 'MODULES.CODE_ANALYSIS.CODE_SECURITY.TITLE',
        icon: 'subdirectory_arrow_right',
        route: '/tutorial/code-analysis/code-security',
        disabled: true
      }
    ]
  },
  {
    title: 'MODULES.TESTING.TITLE',
    icon: 'verified',
    route: '/tutorial/testing',
    children: [
      {
        title: 'MODULES.TESTING.UNIT_TESTING.TITLE',
        icon: 'subdirectory_arrow_right',
        route: '/tutorial/testing/unit-testing'
      },
      {
        title: 'MODULES.TESTING.END_TO_END_TESTING.TITLE',
        icon: 'subdirectory_arrow_right',
        route: '/tutorial/testing/end-to-end-testing'
      }
    ]
  },
  {
    title: 'MODULES.DEVOPS.TITLE',
    icon: 'all_inclusive',
    route: '/tutorial/devops',
    children: [
      {
        title: 'MODULES.ARCHITECTURES.PIPELINE_DIAGRAM.TITLE',
        icon: 'subdirectory_arrow_right',
        route: '/tutorial/devops/pipeline-diagram'
      },
      {
        title: 'MODULES.DEVOPS.CONTINUOUS_INTEGRATION.TITLE',
        icon: 'subdirectory_arrow_right',
        route: '/tutorial/devops/continuous-integration'
      },
      {
        title: 'MODULES.DEVOPS.CONTINUOUS_DELIVERY.TITLE',
        icon: 'subdirectory_arrow_right',
        route: '/tutorial/devops/continuous-delivery'
      },
      {
        title: 'MODULES.DEVOPS.CONTINUOUS_DEPLOYMENT.TITLE',
        icon: 'subdirectory_arrow_right',
        route: '/tutorial/devops/continuous-deployment'
      }
    ]
  },
  {
    title: 'MODULES.STYLES.TITLE',
    icon: 'format_paint',
    route: '/tutorial/styles',
    children: [
      {
        title: 'MODULES.STYLES.RESPONSIVE_DESIGN.TITLE',
        icon: 'subdirectory_arrow_right',
        route: '/tutorial/styles/responsive-design'
      },
      {
        title: 'MODULES.STYLES.THEMES.TITLE',
        icon: 'subdirectory_arrow_right',
        route: '/tutorial/styles/themes'
      },
      {
        title: 'MODULES.STYLES.ICONS.TITLE',
        icon: 'subdirectory_arrow_right',
        route: '/tutorial/styles/icons'
      }
    ]
  },
  {
    title: 'MODULES.LAYOUT.TITLE',
    icon: 'view_compact',
    route: '/tutorial/layout',
    children: [
      {
        title: 'MODULES.LAYOUT.NAVBAR.TITLE',
        icon: 'subdirectory_arrow_right',
        route: '/tutorial/layout/navbar'
      },
      {
        title: 'MODULES.LAYOUT.SIDENAV.TITLE',
        icon: 'subdirectory_arrow_right',
        route: '/tutorial/layout/sidenav'
      },
      {
        title: 'MODULES.LAYOUT.FOOTER.TITLE',
        icon: 'subdirectory_arrow_right',
        route: '/tutorial/layout/footer'
      },
    ]
  },
  {
    title: 'MODULES.CORE.TITLE',
    icon: 'settings',
    route: '/tutorial/core',
    children: [
      {
        title: 'MODULES.CORE.INTERNATIONALIZATION.TITLE',
        icon: 'subdirectory_arrow_right',
        route: '/tutorial/core/internationalization'
      },
      {
        title: 'MODULES.CORE.AUTHENTICATION.TITLE',
        icon: 'subdirectory_arrow_right',
        route: '/tutorial/core/authentication'
      },
      {
        title: 'MODULES.CORE.ERROR_HANDLER.TITLE',
        icon: 'subdirectory_arrow_right',
        route: '/tutorial/core/error-handler'
      },
      {
        title: 'MODULES.CORE.DATABASE.TITLE',
        icon: 'subdirectory_arrow_right',
        route: '/tutorial/core/database',
        disabled: true
      },
      {
        title: 'MODULES.CORE.LOGGER.TITLE',
        icon: 'subdirectory_arrow_right',
        route: '/tutorial/core/logger',
        disabled: true
      }
    ]
  },
  {
    title: 'MODULES.PACKAGES.TITLE',
    icon: 'category',
    route: '/tutorial/packages',
    children: [
      {
        title: 'MODULES.PACKAGES.ANGULAR_MATERIAL.TITLE',
        icon: 'subdirectory_arrow_right',
        route: '/tutorial/packages/angular-material'
      },
      {
        title: 'MODULES.PACKAGES.SCROLLBAR.TITLE',
        icon: 'subdirectory_arrow_right',
        route: '/tutorial/packages/scrollbar'
      },
      {
        title: 'MODULES.PACKAGES.TOASTER.TITLE',
        icon: 'subdirectory_arrow_right',
        route: '/tutorial/packages/toaster'
      },
      {
        title: 'MODULES.PACKAGES.COOKIES.TITLE',
        icon: 'subdirectory_arrow_right',
        route: '/tutorial/packages/cookies'
      },
    ]
  }
];

const HOME_MENU: MenuItem = {
  title: 'MODULES.COMMON.HOME.TITLE',
  icon: 'home',
  route: '/home'
};

export const UNAUTHENTICATED_MENU = [HOME_MENU].concat(TUTORIAL_MENU);

export const AUTHENTICATED_MENU: MenuItem[] = [
  HOME_MENU,
  {
    title: 'Gestion',
    icon: 'inventory',
    route: '/management',
    children: [
      {
        title: 'Utilisateurs',
        icon: 'group',
        route: '/management/user'
      }
    ]
  },
  {
    title: 'Tutoriels',
    icon: 'school',
    route: '/tutorial',
    children: TUTORIAL_MENU
  },
  {
    title: 'Test de récursivité',
    icon: 'assignment',
    route: '/user/test',
    children: [
      {
        title: 'Test 1.1',
        icon: 'assignment',
        route: '/user/test',
      },
      {
        title: 'Test 1.2',
        icon: 'assignment',
        route: '/user/test',
        children: [
          {
            title: 'Test 2.1',
            icon: 'assignment',
            route: '/user/test',
            children: [
              {
                title: 'Test 3.1',
                icon: 'assignment',
                route: '/user/test'
              },
              {
                title: 'Test 3.2',
                icon: 'assignment',
                route: '/user/test'
              }
            ]
          },
          {
            title: 'Test 2.2',
            icon: 'assignment',
            route: '/user/test'
          },
        ]
      }
    ]
  },
];

