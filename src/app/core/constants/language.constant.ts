import { Language } from '@core/interfaces/language';
import { SVG_ICONS_LIST } from '@core/constants/svg-icon.constant';

/**
 * Internationalization languages list for the dropdown selection in navabr.component.ts
 */
export const LANGUAGES_LIST: Language[] = [
  {name: 'ENGLISH', value: 'en', icon: SVG_ICONS_LIST[0]},
  {name: 'FRENCH', value: 'fr', icon: SVG_ICONS_LIST[1]}
];

/**
 * Default language can be modify here
 */
export const DEFAULT_LANGUAGE = LANGUAGES_LIST[0];
