import { Injectable } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { Title } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class BrowserTabTitleService {

  constructor(
    private titleService: Title,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private translateService: TranslateService
  ) {
  }

  public setBrowserTabTitle(): void {
    this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe(() => {
      const activatedRoute = this.getActivateRouteChild(this.activatedRoute);
      activatedRoute.data.subscribe(data => {
        if (data?.title) {
          this.translateService.get('TITLE').subscribe((websiteTitle: string) =>
            this.translateService.get(data.title).subscribe((pageTitle: string) =>
              this.titleService.setTitle(pageTitle + ' - ' + websiteTitle))
          );
        } else {
          this.translateService.get('TITLE').subscribe((websiteTitle: string) =>
            this.titleService.setTitle(websiteTitle));
        }
      });
    });
  }

  private getActivateRouteChild(activatedRoute: ActivatedRoute): ActivatedRoute {
    if (activatedRoute.firstChild) {
      return this.getActivateRouteChild(activatedRoute.firstChild);
    } else {
      return activatedRoute;
    }
  }

}
