import { TestBed } from '@angular/core/testing';

import { InternationalizationService } from './internationalization.service';
import { CoreModule } from '@core/core.module';

describe('InternationalizationService', () => {
  let service: InternationalizationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CoreModule]
    });
    service = TestBed.inject(InternationalizationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
