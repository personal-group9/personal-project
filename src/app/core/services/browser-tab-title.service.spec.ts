import { TestBed } from '@angular/core/testing';

import { BrowserTabTitleService } from './browser-tab-title.service';
import { RouterTestingModule } from '@angular/router/testing';
import { CoreModule } from '@core/core.module';

describe('BrowserTabTitleService', () => {
  let service: BrowserTabTitleService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CoreModule, RouterTestingModule]
    });
    service = TestBed.inject(BrowserTabTitleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
