import { TestBed } from '@angular/core/testing';

import { ScrollbarService } from './scrollbar.service';
import { RouterTestingModule } from '@angular/router/testing';

describe('ScrollbarService', () => {
  let service: ScrollbarService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule]
    });
    service = TestBed.inject(ScrollbarService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
