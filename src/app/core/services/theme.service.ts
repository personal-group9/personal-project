import { ApplicationRef, Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Theme } from '@core/interfaces/theme';
import { DEFAULT_THEME, THEMES_LIST } from '@core/constants/theme.constant';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {

  public selectedTheme: BehaviorSubject<Theme> = new BehaviorSubject(DEFAULT_THEME);

  constructor(private applicationRef: ApplicationRef) {

    /**
     * initially trigger dark mode if preference is set to dark mode on system
     */
    const darkModeOn = window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches;
    if (darkModeOn) {
      this.selectedTheme.next(THEMES_LIST[2]);
    }

    /**
     * watch for changes of the dark mode preference on system
     */
    window.matchMedia('(prefers-color-scheme: dark)').addEventListener('change', listener => {
      this.selectedTheme.next(listener.matches ? THEMES_LIST[2] : THEMES_LIST[0]);
      this.applicationRef.tick();
    });
  }

}
