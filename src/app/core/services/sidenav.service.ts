import { Injectable } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { FIXED_MENU } from '@core/constants/menu.constant';

@Injectable({
  providedIn: 'root'
})
export class SidenavService {

  private sidenav!: MatSidenav;
  public fixedMenu!: boolean;
  private readonly mobileWith = 576;

  public setSidenav(sidenav: MatSidenav): void {
    this.sidenav = sidenav;
    if (window.innerWidth > this.mobileWith) {
      this.fixedMenu = FIXED_MENU;
      if (this.fixedMenu) {
        this.sidenav.open();
      }
    } else {
      this.fixedMenu = false;
    }

  }

  public open(): void {
    this.sidenav.open();
  }

  public close(): void {
    if (!this.fixedMenu) {
      this.sidenav.close();
    }
  }

  public toggle(isOpen?: boolean): void {
    this.sidenav.toggle(isOpen);
  }

  public disableFixedMenuForMobile(): void {
    if (window.innerWidth < this.mobileWith) {
      this.fixedMenu = false;
      this.close();
    }
  }

}
