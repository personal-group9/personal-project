import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Language } from '@core/interfaces/language';
import { DEFAULT_LANGUAGE, LANGUAGES_LIST } from '@core/constants/language.constant';

@Injectable({
  providedIn: 'root'
})
export class InternationalizationService {

  public selectedLanguage = new BehaviorSubject(DEFAULT_LANGUAGE);

  constructor(private translateService: TranslateService) {

    translateService.addLangs(LANGUAGES_LIST.map(language => language.value));

    /**
     * Watch for language changes to set the language
     */
    this.selectedLanguage.subscribe((newLanguage: Language) => translateService.setDefaultLang(newLanguage.value));

    /**
     * Set the brower language if it is available in the application
     */
    const browerLanguage = LANGUAGES_LIST.find(language => language.value === translateService.getBrowserLang());
    if (browerLanguage) {
      this.selectedLanguage.next(browerLanguage);
    }
  }

}
