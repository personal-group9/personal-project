import { Injectable } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ScrollbarService {

  public contentHeight!: number;
  public pageHeightWithoutNavbar!: number;
  private previousRoute!: string;

  /**
   * Modify also this value in styles/variable.scss
   */
  private readonly spaceBetweenComponents: number = 24;
  private readonly navbarHeightPx: number = 56;
  private readonly footerHeightPx: number = 56;

  constructor(private router: Router) {
    this.updateHeight();
    this.resetScrollbarPosition();
  }

  public updateHeight(): void {
    this.pageHeightWithoutNavbar = window.innerHeight - this.navbarHeightPx;
    this.contentHeight = this.pageHeightWithoutNavbar - this.footerHeightPx - (this.spaceBetweenComponents * 2);
  }

  private resetScrollbarPosition(): void {
    this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe(() => {
      const nextRoute = this.router.url.split('#')[0];
      const contentContainer = document.querySelector('.mat-sidenav-content') || window;
      if (this.previousRoute !== nextRoute) {
        contentContainer.scrollTo({top: 0, left: 0, behavior: 'auto'});
      }
      this.previousRoute = nextRoute;
    });
  }

}
