import { Component } from '@angular/core';
import { SetupItem } from '@shared/interfaces/setup-item';
import { Syntax } from '@shared/enums/syntaxs.enum';

@Component({
  selector: 'app-error-handler',
  templateUrl: './error-handler.component.html',
  styleUrls: ['./error-handler.component.scss']
})
export class ErrorHandlerComponent {

  moduleCode = `import { ErrorHandler, NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpErrorInterceptor } from '@core/errors/http-error.interceptor';
import { GlobalErrorHandler } from '@core/errors/global-error-handler';

@NgModule({
  providers: [
    {
      // processes all errors
      provide: ErrorHandler,
      useClass: GlobalErrorHandler
    },
    {
      // interceptor for HTTP errors
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true // multiple interceptors are possible
    }
  ]
})
export class ErrorsModule {
}`;

  localCode = `import { ErrorHandler, Injectable } from '@angular/core';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {

  handleError(error: Error): void {
    console.error('Error from global error handler', error);
  }

}`;

  serverCode = `import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {
        console.error('Error from HTTP error interceptor', error);
        return throwError(error);
      })
    );
  }

}`;

  setupStepList: SetupItem[] = [
    {
      title: 'COMMON.LABELS.MODULE', markdownList: [
        {title: 'src/core/errors/errors.module.ts', syntax: Syntax.TYPESCRIPT, content: this.moduleCode}
      ]
    },
    {
      title: 'MODULES.CORE.ERROR_HANDLER.LOCAL', markdownList: [
        {title: 'src/core/errors/global-error.module.ts', syntax: Syntax.TYPESCRIPT, content: this.localCode}
      ]
    },
    {
      title: 'MODULES.CORE.ERROR_HANDLER.SERVER', markdownList: [
        {title: 'src/core/errors/global-error.module.ts', syntax: Syntax.TYPESCRIPT, content: this.serverCode}
      ]
    }
  ];
}
