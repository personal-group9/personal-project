import { Component } from '@angular/core';
import { SetupItem } from '@shared/interfaces/setup-item';
import { Syntax } from '@shared/enums/syntaxs.enum';

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.scss']
})
export class AuthenticationComponent {

  importCode = `import { NgModule } from '@angular/core';
import { BrowserModule, Title } from '@angular/platform-browser';
import { AppComponent } from '@app/app.component';
import { AppRoutingModule } from '@app/app-routing.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}`;

  envCode = `export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
    authDomain: 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
    databaseURL: 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
    projectId: 'xxxxxxxxxxxxx',
    storageBucket: 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
    messagingSenderId: 'xxxxxxxxxxxxx',
    appId: 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
    measurementId: 'xxxxxxxxxxxxx'
  }
};`;

  envProdCode = `export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
    authDomain: 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
    databaseURL: 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
    projectId: 'xxxxxxxxxxxxx',
    storageBucket: 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
    messagingSenderId: 'xxxxxxxxxxxxx',
    appId: 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
    measurementId: 'xxxxxxxxxxxxx'
  }
};`;

  authGuardCode = `import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '@core/authentication/services/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticatedGuard implements CanActivate {

  constructor(
    public authenticationService: AuthenticationService,
    public router: Router
  ) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (!this.authenticationService.isLoggedIn) {
      this.router.navigate(['/login']);
      return false;
    }
    return true;
  }

}`;

  unauthGuardCode = `import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '@core/authentication/services/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class UnauthenticatedGuard implements CanActivate {

  constructor(
    public authenticationService: AuthenticationService,
    public router: Router
  ) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.authenticationService.isLoggedIn) {
      this.router.navigate(['/home']);
      return false;
    }
    return true;
  }

}`;

  routingCode = `import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from '@app/modules/common/components/login/login.component';
import { ConnectedComponent } from '@app/modules/common/components/connected/connected.component';
import { AuthenticatedGuard } from '@core/authentication/guards/authenticated-guard.service';
import { UnauthenticatedGuard } from '@core/authentication/guards/unauthenticated-guard.service';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [UnauthenticatedGuard]
  }
  {
    path: 'connected',
    component: ConnectedComponent,
    canActivate: [AuthenticatedGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}`;

  authServiceCode = `import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private loggedIn: BehaviorSubject<boolean>;

  constructor(
    public angularFireAuth: AngularFireAuth,
    public router: Router,
    private toastrService: ToastrService,
    public translateService: TranslateService
  ) {
    this.loggedIn = new BehaviorSubject<boolean>(false);
    this.angularFireAuth.authState.subscribe(user => {
      if (user) {
        this.loggedIn.next(true);
        localStorage.setItem('user', JSON.stringify(user));
      } else {
        this.loggedIn.next(false);
        localStorage.setItem('user', '');
      }
    });
  }

  get isLoggedIn(): boolean {
    return this.loggedIn.getValue();
  }

  isLoggedInObservable(): Observable<boolean> {
    return this.loggedIn.asObservable();
  }

  login(email: string, password: string): void {
    this.angularFireAuth.signInWithEmailAndPassword(email, password).then((user) => {
      this.translateService.get('MODULES.COMMON.LOGIN.SUCCESS_LOGIN').subscribe((message: string) => {
        this.toastrService.success(message);
      });
      localStorage.setItem('user', JSON.stringify(user));
      this.router.navigate(['/connected']);
    }).catch(() => {
      this.translateService.get('MODULES.COMMON.LOGIN.ERROR_LOGIN').subscribe((message: string) => this.toastrService.error(message));
    });
  }

  logout(): void {
    this.angularFireAuth.signOut().then(() => {
      localStorage.setItem('user', '');
      this.translateService.get('LAYOUT.NAVBAR.SUCCESS_LOGOUT').subscribe((message: string) => this.toastrService.success(message));
      this.router.navigate(['/home']);
    });
  }

}`;

  usageHtmlCode = `<div>

  <button *ngIf="!authenticationService.isLoggedIn" mat-button routerLink="/login">
    Login
  </button>

  <button (click)="authenticationService.logout()" *ngIf="authenticationService.isLoggedIn" mat-button routerLink="/home">
    Logout
  </button>

  <form (ngSubmit)="onSubmit()" [formGroup]="loginForm">

    <mat-form-field>
      <mat-label>Email</mat-label>
      <input formControlName="email" matInput required type="email">
    </mat-form-field>

    <mat-form-field>
      <mat-label>Password</mat-label>
      <input formControlName="password" matInput required type="password">
    </mat-form-field>

    <button mat-raised-button>Submit</button>

  </form>

</div>`;

  usageTsCode = `import { Component } from '@angular/core';
import { AuthenticationService } from '@core/authentication/services/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  loginForm: FormGroup;

  constructor(
    public authenticationService: AuthenticationService,
  ) {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.email],
      password: ['', Validators.required]
    });
  }

  onSubmit(): void {
    if (this.loginForm.valid) {
      this.authenticationService.login(this.loginForm.get('email')?.value, this.loginForm.get('password')?.value);
    }
  }

}`;

  setupStepList: SetupItem[] = [
    {
      title: 'COMMON.LABELS.DEPENDENCY', markdownList: [
        {title: 'Terminal', syntax: Syntax.BASH, content: 'npm install firebase --save'}
      ]
    },
    {
      title: 'MODULES.CORE.AUTHENTICATION.ENV', markdownList: [
        {title: 'environments/environment.ts', syntax: Syntax.TYPESCRIPT, content: this.envCode},
        {title: 'environments/environment.prod.ts', syntax: Syntax.TYPESCRIPT, content: this.envProdCode}
      ]
    },
    {
      title: 'COMMON.LABELS.MODULE', markdownList: [
        {title: 'src/app/app.module.ts', syntax: Syntax.TYPESCRIPT, content: this.importCode}
      ]
    },
    {
      title: 'MODULES.CORE.AUTHENTICATION.AUTH', markdownList: [
        {title: 'src/app/authenticated/guards/authenticated-guard.service.ts', syntax: Syntax.TYPESCRIPT, content: this.authGuardCode},
        {title: 'src/app/authenticated/unauthenticated-guard.service.ts', syntax: Syntax.TYPESCRIPT, content: this.unauthGuardCode},
        {title: 'src/app/app-routing.module.ts', syntax: Syntax.TYPESCRIPT, content: this.routingCode},
      ]
    },
    {
      title: 'COMMON.LABELS.SERVICE', markdownList: [
        {title: 'src/app/authenticated/services/authentication.service.ts', syntax: Syntax.TYPESCRIPT, content: this.authServiceCode},
      ]
    },
    {
      title: 'COMMON.LABELS.USAGE', markdownList: [
        {title: 'src/app/app.component.ts', syntax: Syntax.TYPESCRIPT, content: this.usageTsCode},
        {title: 'src/app/app.component.html', syntax: Syntax.HTML, content: this.usageHtmlCode}
      ]
    }
  ];

}
