import { Component } from '@angular/core';
import { Language } from '@core/interfaces/language';
import { InternationalizationService } from '@core/services/internationalization.service';
import { SetupItem } from '@shared/interfaces/setup-item';
import { Syntax } from '@shared/enums/syntaxs.enum';
import { LANGUAGES_LIST } from '@core/constants/language.constant';

@Component({
  selector: 'app-internationalization',
  templateUrl: './internationalization.component.html',
  styleUrls: ['./internationalization.component.scss']
})
export class InternationalizationComponent {

  importCode = `import { NgModule } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

export function httpTranslateLoader(http: HttpClient): any {
  return new TranslateHttpLoader(http);
}

@NgModule({
  imports: [
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: httpTranslateLoader,
        deps: [HttpClient]
      }
    })
  ]
})

export class AppModule { }`;

  modelCode = `export interface Language {
  name: string;
  value: string;
  icon?: string;
}`;

  constantCode = `import { Language } from '@core/interfaces/language';

/**
 * Internationalization languages list for the dropdown selection in navabr.component.ts
 */
export const LANGUAGES_LIST: Language[] = [
  {name: 'ENGLISH', value: 'en'},
  {name: 'FRENCH', value: 'fr'}
];

/**
 * Default language can be modify here
 */
export const DEFAULT_LANGUAGE = LANGUAGES_LIST[0];`;

  serviceCode = `import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { LANGUAGES_LIST, DEFAULT_LANGUAGE } from '@core/constants/language.constant';
import { TranslateService } from '@ngx-translate/core';
import { Language } from '@core/interfaces/language';

@Injectable({
  providedIn: 'root'
})
export class InternationalizationService {

  public selectedLanguage = new BehaviorSubject(DEFAULT_LANGUAGE);

  constructor(private translateService: TranslateService) {

    translateService.addLangs(LANGUAGES_LIST.map(language => language.value));

    /**
     * Watch for language changes to set the language
     */
    this.selectedLanguage.subscribe((newLanguage: Language) => translateService.setDefaultLang(newLanguage.value));

    /**
     * Set the brower language if it is available in the application
     */
    const browerLanguage = LANGUAGES_LIST.find(language => language.value === translateService.getBrowserLang());
    if (browerLanguage) {
      this.selectedLanguage.next(browerLanguage);
    }
  }

}`;

  htmlComponentCode = `<div>
  {{ 'LABELS.HELLO_WORLD' | translate }}
<div>

<button [matMenuTriggerFor]="languagesMenu" mat-button>
  <mat-icon svgIcon="{{ internationalizationService.selectedLanguage.getValue().icon?.name }}"></mat-icon>
  {{ 'LANGUAGES.' + internationalizationService.selectedLanguage.getValue().name | translate }}
  <mat-icon>arrow_drop_down</mat-icon>
</button>

<mat-menu #languagesMenu="matMenu">
  <button (click)="changeLanguage(language)" *ngFor="let language of languages" mat-menu-item>
    <mat-icon svgIcon="{{ language.icon?.name }}"></mat-icon>
    {{ 'LANGUAGES.' + language.name | translate }}
  </button>
</mat-menu>`;

  enTranslateCode = `{
    "LABELS": {
        "HELLO_WORLD": "Hello world"
    },
    "LANGUAGES": {
        "ENGLISH": "English",
        "FRENCH": "French"
    }
}`;

  frTranslateCode = `{
    "LABELS": {
        "HELLO_WORLD": "Bonjour le monde"
    },
    "LANGUAGES": {
        "ENGLISH": "Anglais",
        "FRENCH": "Français"
    }
}`;

  typescriptComponentCode = `import { Component } from '@angular/core';
import { LANGUAGES_LIST } from '@core/constants/language.constant';
import { Language } from '@core/interfaces/language';
import { InternationalizationService } from '@core/services/internationalization.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {

  languages = LANGUAGES_LIST;

  constructor(
    public internationalizationService: InternationalizationService
  ) { }

  changeLanguage(language: Language): void {
    this.internationalizationService.selectedLanguage.next(language);
  }

}`;

  setupStepList: SetupItem[] = [
    {
      title: 'COMMON.LABELS.DEPENDENCY', markdownList: [
        {title: 'Terminal', syntax: Syntax.BASH, content: 'npm install @ngx-translate/core --save'}
      ]
    },
    {
      title: 'COMMON.LABELS.MODULE', markdownList: [
        {title: 'src/app/app.module.ts', syntax: Syntax.TYPESCRIPT, content: this.importCode}
      ]
    },
    {
      title: 'MODULES.CORE.INTERNATIONALIZATION.JSON_FILES_TRANSLATION', markdownList: [
        {title: 'src/assets/i18n/en.ts', syntax: Syntax.JSON, content: this.enTranslateCode},
        {title: 'src/assets/i18n/fr.ts', syntax: Syntax.JSON, content: this.frTranslateCode}
      ]
    },
    {
      title: 'MODULES.CORE.INTERNATIONALIZATION.LANGUAGES', markdownList: [
        {title: 'src/app/core/interfaces/language.ts', syntax: Syntax.TYPESCRIPT, content: this.modelCode},
        {title: 'src/app/core/constants/language.constant.ts', syntax: Syntax.TYPESCRIPT, content: this.constantCode}
      ]
    },
    {
      title: 'MODULES.CORE.INTERNATIONALIZATION.SERVICE', markdownList: [
        {title: 'src/app/core/services/internationalization.service.ts', syntax: Syntax.TYPESCRIPT, content: this.serviceCode}
      ]
    },
    {
      title: 'COMMON.LABELS.USAGE', markdownList: [
        {title: 'src/app/app.component.ts', syntax: Syntax.TYPESCRIPT, content: this.typescriptComponentCode},
        {title: 'src/app/app.component.html', syntax: Syntax.HTML, content: this.htmlComponentCode}
      ]
    }
  ];

  languages = LANGUAGES_LIST;

  constructor(public internationalizationService: InternationalizationService) {
  }

  changeLanguage(language: Language): void {
    this.internationalizationService.selectedLanguage.next(language);
  }

}
