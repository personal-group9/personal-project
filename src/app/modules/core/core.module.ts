import { NgModule } from '@angular/core';
import { InternationalizationComponent } from '@app/modules/core/components/internationalization/internationalization.component';
import { DatabaseComponent } from '@app/modules/core/components/database/database.component';
import { ErrorHandlerComponent } from '@app/modules/core/components/error-handler/error-handler.component';
import { AuthenticationComponent } from '@app/modules/core/components/authentication/authentication.component';
import { LoggerComponent } from '@app/modules/core/components/logger/logger.component';
import { SharedModule } from '@shared/shared.module';
import { CoreRoutingModule } from '@app/modules/core/core-routing.module';

@NgModule({
  declarations: [
    InternationalizationComponent,
    DatabaseComponent,
    AuthenticationComponent,
    ErrorHandlerComponent,
    LoggerComponent
  ],
  imports: [
    CoreRoutingModule,
    SharedModule
  ]
})
export class CoreModule {
}
