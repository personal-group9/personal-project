import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InternationalizationComponent } from '@app/modules/core/components/internationalization/internationalization.component';
import { DatabaseComponent } from '@app/modules/core/components/database/database.component';
import { AuthenticationComponent } from '@app/modules/core/components/authentication/authentication.component';
import { ErrorHandlerComponent } from '@app/modules/core/components/error-handler/error-handler.component';
import { LoggerComponent } from '@app/modules/core/components/logger/logger.component';
import { NotFoundComponent } from '@app/modules/home/components/not-found/not-found.component';

const coreRoutes: Routes = [
  {
    path: 'internationalization',
    component: InternationalizationComponent,
    data: {title: 'MODULES.CORE.INTERNATIONALIZATION.TITLE'}
  },
  {
    path: 'database',
    component: DatabaseComponent,
    data: {title: 'MODULES.CORE.DATABASE.TITLE'}
  },
  {
    path: 'authentication',
    component: AuthenticationComponent,
    data: {title: 'MODULES.CORE.AUTHENTICATION.TITLE'}
  },
  {
    path: 'error-handler',
    component: ErrorHandlerComponent,
    data: {title: 'MODULES.CORE.ERROR_HANDLER.TITLE'}
  },
  {
    path: 'logger',
    component: LoggerComponent,
    data: {title: 'MODULES.CORE.LOGGER.TITLE'}
  },
  {
    path: '**',
    component: NotFoundComponent,
    data: {title: 'MODULES.COMMON.NOT_FOUND.TITLE'}
  }
];

@NgModule({
  imports: [RouterModule.forChild(coreRoutes)],
  exports: [RouterModule]
})
export class CoreRoutingModule {
}
