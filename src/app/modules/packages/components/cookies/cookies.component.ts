import { Component } from '@angular/core';
import { SetupItem } from '@shared/interfaces/setup-item';
import { Syntax } from '@shared/enums/syntaxs.enum';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-cookies',
  templateUrl: './cookies.component.html',
  styleUrls: ['./cookies.component.scss']
})
export class CookiesComponent {

  importCode = `import { NgModule } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@NgModule({
  providers: [ CookieService ]
})

export class AppModule { }`;

  usageCode = `import { Component } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class NavbarComponent {

  constructor( private cookieService: CookieService ) {
    this.cookieService.set( 'Test', 'Hello World' );
    this.cookieValue = this.cookieService.get('Test');
  }

}`;

  setupStepList: SetupItem[] = [
    {
      title: 'COMMON.LABELS.DEPENDENCY', markdownList: [
        {title: 'Terminal', syntax: Syntax.BASH, content: 'npm install ngx-cookie-service --save'}
      ]
    },
    {
      title: 'COMMON.LABELS.MODULE', markdownList: [
        {title: 'src/app/app.module.ts', syntax: Syntax.TYPESCRIPT, content: this.importCode}
      ]
    },
    {
      title: 'COMMON.LABELS.USAGE', markdownList: [
        {title: 'src/app/app.component.ts', syntax: Syntax.TYPESCRIPT, content: this.usageCode}
      ]
    }
  ];

  cookieValue: string;

  constructor(private cookieService: CookieService) {
    this.cookieValue = this.cookieService.get('Test');
  }

  onRefresh(value: string): void {
    this.cookieService.set('Test', value.toString());
    location.reload();
  }

}
