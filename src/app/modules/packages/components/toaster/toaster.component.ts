import { Component } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { SetupItem } from '@shared/interfaces/setup-item';
import { Syntax } from '@shared/enums/syntaxs.enum';

@Component({
  selector: 'app-toaster',
  templateUrl: './toaster.component.html',
  styleUrls: ['./toaster.component.scss']
})
export class ToasterComponent {

  constructor(public toastrService: ToastrService) {
  }

  importCode = `import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }`;

  usageCode = `import { Component } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class NavbarComponent {

  constructor(public toastrService: ToastrService) {}

  showToaster() {
    this.toastrService.success('Hello world!', 'Toastr fun!');
    this.toastrService.warning('Hello world!', 'Toastr fun!');
    this.toastrService.error('Hello world!', 'Toastr fun!');
    this.toastrService.info('Hello world!', 'Toastr fun!');
  }

}`;

  addCode = `{
  "$schema": "./node_modules/@angular/cli/lib/config/schema.json",
  "version": 1,
  "newProjectRoot": "projects",
  "projects": {
    "personal-project": {
      ...
      "architect": {
        "build": {
          ...
          "options": {
            ...
            "styles": [
              "node_modules/ngx-toastr/toastr.css"
            ]
          }
        }
      }
    }
  }
}`;

  usageHTMLCode = `<p>
  <button (click)="toastrService.info('This is a info message')" mat-raised-button>
    Info toaster
  </button>
  <button (click)="toastrService.success('This is a success message')" color="primary" mat-raised-button>
    Success toaster
  </button>
  <button (click)="toastrService.warning('This is a warning message')" color="accent" mat-raised-button>
    Warning toaster
  </button>
  <button (click)="toastrService.error('This is a error message')" mat-raised-button>Error
    toaster
  </button>
</p>`;

  usageCSSCode = `/* All CSS toaster to change icons and color */

.toast-warning {
  background-image: url('~assets/icons/warning-white.svg');
}

.toast-error {
  background-image: url('~assets/icons/error-white.svg');
}

.toast-info {
  background-image: url('~assets/icons/info-white.svg');
}

.toast-success {
  background-image: url('~assets/icons/done-white.svg');
}
`;

  setupStepList: SetupItem[] = [
    {
      title: 'COMMON.LABELS.DEPENDENCY', markdownList: [
        {title: 'Terminal', syntax: Syntax.BASH, content: 'npm install ngx-toastr --save'}
      ]
    },
    {
      title: 'COMMON.LABELS.CSS', markdownList: [
        {title: 'angular.json', syntax: Syntax.JSON, content: this.addCode}
      ]
    },
    {
      title: 'COMMON.LABELS.MODULE', markdownList: [
        {title: 'src/app/app.module.ts', syntax: Syntax.TYPESCRIPT, content: this.importCode}
      ]
    },
    {
      title: 'COMMON.LABELS.USAGE', markdownList: [
        {title: 'src/app/app.component.ts', syntax: Syntax.TYPESCRIPT, content: this.usageCode},
        {title: 'src/app/app.component.html', syntax: Syntax.HTML, content: this.usageHTMLCode},
        {title: 'src/app/app.component.scss', syntax: Syntax.SCSS, content: this.usageCSSCode}
      ]
    }
  ];

}
