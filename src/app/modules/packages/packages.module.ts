import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { ScrollbarComponent } from '@app/modules/packages/components/scrollbar/scrollbar.component';
import { ToasterComponent } from '@app/modules/packages/components/toaster/toaster.component';
import { PackagesRoutingModule } from '@app/modules/packages/packages-routing.module';
import { CookiesComponent } from './components/cookies/cookies.component';
import { AngularMaterialComponent } from './components/angular-material/angular-material.component';

@NgModule({
  declarations: [
    ScrollbarComponent,
    ToasterComponent,
    CookiesComponent,
    AngularMaterialComponent
  ],
  imports: [
    PackagesRoutingModule,
    SharedModule
  ]
})
export class PackagesModule {
}
