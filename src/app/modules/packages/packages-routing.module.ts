import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ScrollbarComponent } from '@app/modules/packages/components/scrollbar/scrollbar.component';
import { ToasterComponent } from '@app/modules/packages/components/toaster/toaster.component';
import { CookiesComponent } from '@app/modules/packages/components/cookies/cookies.component';
import { AngularMaterialComponent } from '@app/modules/packages/components/angular-material/angular-material.component';
import { NotFoundComponent } from '@app/modules/home/components/not-found/not-found.component';

const componentsRoutes: Routes = [
  {
    path: 'angular-material',
    component: AngularMaterialComponent,
    data: {title: 'MODULES.PACKAGES.ANGULAR_MATERIAL.TITLE'}
  },
  {
    path: 'scrollbar',
    component: ScrollbarComponent,
    data: {title: 'MODULES.PACKAGES.SCROLLBAR.TITLE'}
  },
  {
    path: 'toaster',
    component: ToasterComponent,
    data: {title: 'MODULES.PACKAGES.TOASTER.TITLE'}
  },
  {
    path: 'cookies',
    component: CookiesComponent,
    data: {title: 'MODULES.PACKAGES.COOKIES.TITLE'}
  },
  {
    path: '**',
    component: NotFoundComponent,
    data: {title: 'MODULES.COMMON.NOT_FOUND.TITLE'}
  }
];

@NgModule({
  imports: [RouterModule.forChild(componentsRoutes)],
  exports: [RouterModule]
})
export class PackagesRoutingModule {
}
