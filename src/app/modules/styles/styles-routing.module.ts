import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResponsiveDesignComponent } from '@app/modules/styles/components/responsive-design/responsive-design.component';
import { ThemesComponent } from '@app/modules/styles/components/themes/themes.component';
import { IconsComponent } from '@app/modules/styles/components/icons/icons.component';
import { NotFoundComponent } from '@app/modules/home/components/not-found/not-found.component';

const stylesRoutes: Routes = [
  {
    path: 'responsive-design',
    component: ResponsiveDesignComponent,
    data: {title: 'MODULES.STYLES.RESPONSIVE_DESIGN.TITLE'}
  },
  {
    path: 'themes',
    component: ThemesComponent,
    data: {title: 'MODULES.STYLES.THEMES.TITLE'}
  },
  {
    path: 'icons',
    component: IconsComponent,
    data: {title: 'MODULES.STYLES.ICONS.TITLE'}
  },
  {
    path: '**',
    component: NotFoundComponent,
    data: {title: 'MODULES.COMMON.NOT_FOUND.TITLE'}
  }
];

@NgModule({
  imports: [RouterModule.forChild(stylesRoutes)],
  exports: [RouterModule]
})
export class StylesRoutingModule {
}
