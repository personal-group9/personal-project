import { NgModule } from '@angular/core';
import { ResponsiveDesignComponent } from '@app/modules/styles/components/responsive-design/responsive-design.component';
import { ThemesComponent } from '@app/modules/styles/components/themes/themes.component';
import { IconsComponent } from '@app/modules/styles/components/icons/icons.component';
import { SharedModule } from '@shared/shared.module';
import { StylesRoutingModule } from '@app/modules/styles/styles-routing.module';

@NgModule({
  declarations: [
    ResponsiveDesignComponent,
    ThemesComponent,
    IconsComponent
  ],
  imports: [
    StylesRoutingModule,
    SharedModule
  ]
})
export class StylesModule {
}
