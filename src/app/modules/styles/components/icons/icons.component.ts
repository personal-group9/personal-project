import { Component } from '@angular/core';
import { SetupItem } from '@shared/interfaces/setup-item';
import { Syntax } from '@shared/enums/syntaxs.enum';

@Component({
  selector: 'app-icons',
  templateUrl: './icons.component.html',
  styleUrls: ['./icons.component.scss']
})
export class IconsComponent {

  usageCode = `<!--class usage-->
  <span class="material-icons">home</span>
  <span class="material-icons icon-size-24">home</span>
  <span class="material-icons-outlined">home</span>
  <span class="material-icons-two-tone">home</span>
  <span class="material-icons-sharp">home</span>
  <span class="material-icons-round">home</span>

  <!--mat-icon html tag usage-->
  <mat-icon>home</mat-icon>
  <mat-icon class="icon-size-24">home</mat-icon>
  <mat-icon class="material-icons">home</mat-icon>
  <mat-icon class="material-icons-outlined">home</mat-icon>
  <mat-icon class="material-icons-two-tone">home</mat-icon>
  <mat-icon class="material-icons-sharp">home</mat-icon>
  <mat-icon class="material-icons-round">home</mat-icon>`;

  usageCode2 = `<mat-icon class="icon-size-24" svgIcon="english-flag"></mat-icon>`;

  // noinspection HtmlUnknownTarget
  indexCoce = `<!doctype html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <title>Personal Project</title>
    <base href="/">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="assets/favicon.ico" rel="icon" type="image/x-icon">
    <!--Google Web Fonts-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  </head>

  <body>
    <app-root></app-root>
  </body>

</html>`;

  npmCode = `{
  "$schema": "./node_modules/@angular/cli/lib/config/schema.json",
  "version": 1,
  "newProjectRoot": "projects",
  "projects": {
    "personal-project": {
      ...
      "architect": {
        "build": {
          ...
          "options": {
            ...
            "styles": [
              "node_modules/material-design-icons/iconfont/material-icons.css"
            ]
          }
        }
      }
    }
  }
}`;

  localCode = `@font-face {
  font-family: 'Material Icons';
  font-style: normal;
  font-weight: 400;
  src: url(src/assets/material-design-icons/font/MaterialIcons-Regular.eot); /* For IE6-8 */
  src: local('Material Icons'),
    local('MaterialIcons-Regular'),
    url(src/assets/material-design-icons/font/MaterialIcons-Regular.woff2) format('woff2'),
    url(src/assets/material-design-icons/font/MaterialIcons-Regular.woff) format('woff'),
    url(src/assets/material-design-icons/font/MaterialIcons-Regular.ttf) format('truetype');
}

.material-icons {
  font-family: 'Material Icons';
  font-weight: normal;
  font-style: normal;
  font-size: 24px;  /* Preferred icon size */
  display: inline-block;
  line-height: 1;
  text-transform: none;
  letter-spacing: normal;
  word-wrap: normal;
  white-space: nowrap;
  direction: ltr;

  /* Support for all WebKit browsers. */
  -webkit-font-smoothing: antialiased;
  /* Support for Safari and Chrome. */
  text-rendering: optimizeLegibility;

  /* Support for Firefox. */
  -moz-osx-font-smoothing: grayscale;

  /* Support for IE. */
  font-feature-settings: 'liga';

  /* Rules for sizing the icon. */
  .icon-size-18 { font-size: 18px; height: 18px !important; width: 18px !important }
  .icon-size-24 { font-size: 24px; height: 24px !important; width: 24px !important }
  .icon-size-36 { font-size: 36px; height: 36px !important; width: 36px !important }
  .icon-size-48 { font-size: 48px; height: 48px !important; width: 48px !important }
}`;

  cssCoce = `/* Rules for sizing the icon. */
.icon-size-18 { font-size: 18px; height: 18px !important; width: 18px !important }
.icon-size-24 { font-size: 24px; height: 24px !important; width: 24px !important }
.icon-size-36 { font-size: 36px; height: 36px !important; width: 36px !important }
.icon-size-48 { font-size: 48px; height: 48px !important; width: 48px !important }`;

  frenchFlagCode = `<svg id="flag-icon-css-fr" viewBox="0 0 640 480" xmlns="http://www.w3.org/2000/svg">
  <g fill-rule="evenodd" stroke-width="1pt">
    <path d="M0 0h640v480H0z" fill="#fff"/>
    <path d="M0 0h213.3v480H0z" fill="#00267f"/>
    <path d="M426.7 0H640v480H426.7z" fill="#f31830"/>
  </g>
</svg>
`;

  englishFlagCode = `<svg id="flag-icon-css-gb" viewBox="0 0 640 480" xmlns="http://www.w3.org/2000/svg">
  <path d="M0 0h640v480H0z" fill="#012169"/>
  <path d="M75 0l244 181L562 0h78v62L400 241l240 178v61h-80L320 301 81 480H0v-60l239-178L0 64V0h75z" fill="#FFF"/>
  <path
    d="M424 281l216 159v40L369 281h55zm-184 20l6 35L54 480H0l240-179zM640 0v3L391 191l2-44L590 0h50zM0 0l239 176h-60L0 42V0z"
    fill="#C8102E"/>
  <path d="M241 0v480h160V0H241zM0 160v160h640V160H0z" fill="#FFF"/>
  <path d="M0 193v96h640v-96H0zM273 0v480h96V0h-96z" fill="#C8102E"/>
</svg>`;

  modelCode = `export interface SvgIcon {
  name: string;
  url: string;
}`;

  listCode = `import { SvgIcon } from '@core/interfaces/svg-icon';

/**
 * Define all svg icons
 */
export const SVG_ICONS_LIST: SvgIcon[] = [
  {name: 'english-flag', url: '../assets/icons/english-flag.svg'},
  {name: 'french-flag', url: '../assets/icons/french-flag.svg'}
];`;

  svgAddCode = `import { Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { SVG_ICONS_LIST } from '@core/constants/svg-icon.constant';
import { SvgIcon } from '@core/interfaces/svg-icon';
import { MatIconRegistry } from '@angular/material/icon';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer
  ) {
    this.addSvgIcons();
  }

  private addSvgIcons(): void {
    SVG_ICONS_LIST.forEach((icon: SvgIcon) => {
      this.matIconRegistry.addSvgIcon(icon.name, this.domSanitizer.bypassSecurityTrustResourceUrl(icon.url));
    });
  }

}`;

  stylesCode = `/** FIXME
  Unofficial npm package form google icons :
  https://www.npmjs.com/package/material-icons

  The Official one is not maintained :
  https://github.com/google/material-design-icons/issues/1129
  https://www.npmjs.com/package/material-design-icons
 */
@import "~material-icons/iconfont/material-icons.css";`;

  npmStepList: SetupItem[] = [
    {
      title: 'COMMON.LABELS.DEPENDENCY', markdownList: [
        {title: 'src/index.html', syntax: Syntax.BASH, content: 'npm install material-design-icons --save'}
      ]
    },
    {
      title: 'COMMON.LABELS.MODULE', markdownList: [
        {title: 'angular.json', syntax: Syntax.JSON, content: this.npmCode}
      ]
    },
    {
      title: 'COMMON.LABELS.USAGE', markdownList: [
        {title: 'src/app/app.component.html', syntax: Syntax.HTML, content: this.usageCode},
        {title: 'src/styles/icons.scss', syntax: Syntax.SCSS, content: this.cssCoce}
      ]
    }
  ];

  npmUnofficalStepList: SetupItem[] = [
    {
      title: 'COMMON.LABELS.DEPENDENCY', description: `<ul>
    <li>
      <a href="https://www.npmjs.com/package/material-icons" rel="noopener" target="_blank">https://www.npmjs.com/package/material-icons</a>
    </li>
    <li>
      <a href="https://github.com/marella/material-icons" rel="noopener" target="_blank">https://github.com/marella/material-icons</a>
    </li>
    <li>
      <a href="https://snyk.io/advisor/npm-package/material-icons" rel="noopener" target="_blank">https://snyk.io/advisor/npm-package/material-icons</a>
    </li>
  </ul>`, markdownList: [
        {title: 'src/index.html', syntax: Syntax.BASH, content: 'npm install material-icons --save'}
      ]
    },
    {
      title: 'COMMON.LABELS.MODULE', markdownList: [
        {title: 'styles.scss', syntax: Syntax.SCSS, content: this.stylesCode}
      ]
    },
    {
      title: 'COMMON.LABELS.USAGE', markdownList: [
        {title: 'src/app/app.component.html', syntax: Syntax.HTML, content: this.usageCode},
        {title: 'src/styles/icons.scss', syntax: Syntax.SCSS, content: this.cssCoce}
      ]
    }
  ];

  webStepList: SetupItem[] = [
    {
      title: 'COMMON.LABELS.SET_UP', markdownList: [
        {title: 'src/index.html', syntax: Syntax.HTML, content: this.indexCoce},
      ]
    },
    {
      title: 'COMMON.LABELS.USAGE', markdownList: [
        {title: 'src/app/app.component.html', syntax: Syntax.HTML, content: this.usageCode},
        {title: 'src/styles/icons.scss', syntax: Syntax.SCSS, content: this.cssCoce}
      ]
    }
  ];

  localStepList: SetupItem[] = [
    {
      title: 'COMMON.LABELS.DOWNLOAD', description: 'MODULES.STYLES.ICONS.DOWNLOAD'
    },
    {
      title: 'MODULES.STYLES.ICONS.CSS', markdownList: [
        {title: 'src/styles/icons.scss', syntax: Syntax.SCSS, content: this.localCode}
      ]
    },
    {
      title: 'COMMON.LABELS.USAGE', markdownList: [
        {title: 'src/app/app.component.html', syntax: Syntax.HTML, content: this.usageCode},
      ]
    }
  ];

  svgStepList: SetupItem[] = [
    {
      title: 'MODULES.STYLES.ICONS.ADD_SVG', markdownList: [
        {title: 'src/assets/icons/french-flag.svg', syntax: Syntax.HTML, content: this.frenchFlagCode},
        {title: 'src/assets/icons/english-flag.svg', syntax: Syntax.HTML, content: this.englishFlagCode}
      ]
    },
    {
      title: 'MODULES.STYLES.ICONS.MODEL_LIST', markdownList: [
        {title: 'src/app/core/model/svg-icon.ts', syntax: Syntax.TYPESCRIPT, content: this.modelCode},
        {title: 'src/app/core/constants/svg-icon.constant.ts', syntax: Syntax.TYPESCRIPT, content: this.listCode}
      ]
    },
    {
      title: 'MODULES.STYLES.ICONS.IMPORT', markdownList: [
        {title: 'src/app/app.component.ts', syntax: Syntax.TYPESCRIPT, content: this.svgAddCode},
      ]
    },
    {
      title: 'COMMON.LABELS.USAGE', markdownList: [
        {title: 'src/app/app.component.html', syntax: Syntax.HTML, content: this.usageCode2},
        {title: 'src/styles/icons.scss', syntax: Syntax.SCSS, content: this.cssCoce}
      ]
    }
  ];
}
