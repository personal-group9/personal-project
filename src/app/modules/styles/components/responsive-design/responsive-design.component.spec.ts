import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResponsiveDesignComponent } from './responsive-design.component';
import { CoreModule } from '@core/core.module';
import { SharedModule } from '@shared/shared.module';
import { RouterTestingModule } from '@angular/router/testing';

describe('ResponsiveDesignComponent', () => {
  let component: ResponsiveDesignComponent;
  let fixture: ComponentFixture<ResponsiveDesignComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CoreModule, SharedModule, RouterTestingModule],
      declarations: [ResponsiveDesignComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResponsiveDesignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
