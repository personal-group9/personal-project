import { Component } from '@angular/core';
import { SetupItem } from '@shared/interfaces/setup-item';
import { Syntax } from '@shared/enums/syntaxs.enum';

@Component({
  selector: 'app-responsive-design',
  templateUrl: './responsive-design.component.html',
  styleUrls: ['./responsive-design.component.scss']
})
export class ResponsiveDesignComponent {

  importCode = `{
  "$schema": "./node_modules/@angular/cli/lib/config/schema.json",
  "version": 1,
  "newProjectRoot": "projects",
  "projects": {
    "personal-project": {
      ...
      "architect": {
        "build": {
          ...
          "options": {
            ...
            "styles": [
              "node_modules/bootstrap/scss/bootstrap-grid.scss"
            ]
          }
        }
      }
    }
  }
}`;

  usageCode = `<div class="container">
  <div class="row">
    <div class="col-sm">
      One of three columns
    </div>
    <div class="col-sm">
      One of three columns
    </div>
    <div class="col-sm">
      One of three columns
    </div>
  </div>
</div>`;

  setupStepList: SetupItem[] = [
    {
      title: 'COMMON.LABELS.DEPENDENCY', markdownList: [
        {title: 'Terminal', syntax: Syntax.BASH, content: 'npm install bootstrap@next --save'}
      ]
    },
    {
      title: 'COMMON.LABELS.CSS', description: 'MODULES.STYLES.RESPONSIVE_DESIGN.IMPORT', markdownList: [
        {title: 'angular.json', syntax: Syntax.JSON, content: this.importCode}
      ]
    },
    {
      title: 'COMMON.LABELS.USAGE', markdownList: [
        {title: 'src/app/app.component.html', syntax: Syntax.HTML, content: this.usageCode}
      ]
    }
  ];

}
