import { Component } from '@angular/core';
import { SetupItem } from '@shared/interfaces/setup-item';
import { Syntax } from '@shared/enums/syntaxs.enum';
import { Theme } from '@core/interfaces/theme';
import { ThemeService } from '@core/services/theme.service';
import { THEMES_LIST } from '@core/constants/theme.constant';

@Component({
  selector: 'app-themes',
  templateUrl: './themes.component.html',
  styleUrls: ['./themes.component.scss']
})
export class ThemesComponent {

  cssCode = `@import '~@angular/material/theming';

@include mat-core();

// Custom elements
@mixin custom-components-theme($theme) {
  $accent: map-get($theme, accent);
  .active-item {
    color: mat-color($accent, 400) !important;
    background: rgba(0, 0, 0, .12) !important;
  }
  a {
    word-break: break-word;
    color: mat-color($accent, 400) !important;
  }
}

// Classic theme
$classic-primary: mat-palette($mat-indigo);
$classic-accent: mat-palette($mat-indigo, 900);
$classic-warn: mat-palette($mat-pink, 600);
$classic-theme: mat-light-theme($classic-primary, $classic-accent, $classic-warn);
.classic-theme {
  @include angular-material-theme($classic-theme);
  @include custom-components-theme($classic-theme);
}

// Light theme
$light-primary: mat-palette($mat-grey, 200, 500, 300);
$light-accent: mat-palette($mat-brown, 200);
$light-warn: mat-palette($mat-deep-orange, 200);
$light-theme: mat-light-theme($light-primary, $light-accent, $light-warn);
.light-theme {
  @include angular-material-theme($light-theme);
  @include custom-components-theme($light-theme);
}

// Dark theme
$dark-primary: mat-palette($mat-grey, 700, 300, 900);
$dark-accent: mat-palette($mat-blue-grey, 400);
$dark-warn: mat-palette($mat-red, 500);
$dark-theme: mat-dark-theme($dark-primary, $dark-accent, $dark-warn);
.dark-theme {
  @include angular-material-theme($dark-theme);
  @include custom-components-theme($dark-theme);
}`;

  modelCode = `export interface Theme {
  name: string;
  value: string;
}`;

  listCode = `import { Theme } from '@core/interfaces/theme';

/**
 * All themes define in @app/styles/themes.scss
 */
export const THEMES_LIST: Theme[] = [
  {name: 'Classic', value: 'classic-theme'},
  {name: 'Light', value: 'light-theme'},
  {name: 'Dark', value: 'dark-theme'}
];

/**
 * Default theme can be modify here
 */
export const DEFAULT_THEME: Theme = THEMES_LIST[0];`;

  serviceCode = `import { ApplicationRef, Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { THEMES_LIST, DEFAULT_THEME } from '@core/constants/theme.constant';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {

  public selectedTheme = new BehaviorSubject(DEFAULT_THEME);

  constructor(private applicationRef: ApplicationRef) {

    /**
     * initially trigger dark mode if preference is set to dark mode on system
     */
    const darkModeOn = window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches;
    if (darkModeOn) {
      this.selectedTheme.next(THEMES_LIST[2]);
    }

    /**
     * watch for changes of the dark mode preference on system
     */
    window.matchMedia('(prefers-color-scheme: dark)').addEventListener('change', listener => {
      this.selectedTheme.next(listener.matches ? THEMES_LIST[2] : THEMES_LIST[0]);
      this.applicationRef.tick();
    });
  }

}`;

  updateCode = `import { Component, HostBinding } from '@angular/core';
import { ThemeService } from '@core/services/theme.service';
import { OverlayContainer } from '@angular/cdk/overlay';
import { THEMES_LIST } from '@core/constants/theme.constant';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  @HostBinding('class') public cssClass!: string;

  constructor(
    private themeService: ThemeService,
    private overlayContainer: OverlayContainer
  ) {
    this.updateTheme();
  }

  private updateTheme(): void {
    this.themeService.selectedTheme.subscribe((theme: Theme) => {
      this.cssClass = theme.value;
      this.applyThemeOnOverlays();
    });
  }

  /**
   * Apply the current theme on components with overlay (e.g. Dropdowns, Dialogs)
   */
  private applyThemeOnOverlays(): void {
    this.overlayContainer.getContainerElement().classList.remove(...THEMES_LIST.map(theme => theme.value));
    this.overlayContainer.getContainerElement().classList.add(this.cssClass);
  }

}`;

  usageHTMLCode = `<button [matMenuTriggerFor]="themesMenu" mat-button>
  <mat-icon>format_color_fill</mat-icon>
</button>

<mat-menu #themesMenu="matMenu">
  <button *ngFor="let theme of themes" (click)="changeTheme(theme)" mat-menu-item>
    theme.name
  </button>
</mat-menu>`;

  usageTypescriptCode = `import { Component } from '@angular/core';
import { ThemeService } from '@core/services/theme.service';
import { THEMES_LIST } from '@core/constants/theme.constant';
import { Theme } from '@core/interfaces/theme';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {

  themes = THEMES_LIST;

  constructor(
    public themeService: ThemeService,
  ) { }

  changeTheme(theme: Theme): void {
    this.themeService.selectedTheme.next(theme);
  }

}`;

  setupStepList: SetupItem[] = [
    {
      title: 'MODULES.STYLES.THEMES.ADD_CSS', markdownList: [
        {title: 'src/styles/themes.scss', syntax: Syntax.SCSS, content: this.cssCode}
      ]
    },
    {
      title: 'MODULES.STYLES.THEMES.MODEL_LIST', markdownList: [
        {title: 'src/app/core/interfaces/theme.ts', syntax: Syntax.TYPESCRIPT, content: this.modelCode},
        {title: 'src/app/core/constants/theme.constant.ts', syntax: Syntax.TYPESCRIPT, content: this.listCode}
      ]
    },
    {
      title: 'MODULES.STYLES.THEMES.SERVICE', description: 'MODULES.STYLES.THEMES.SERVICE_DESCRIPTION', markdownList: [
        {title: 'src/app/core/services/theme.service.ts', syntax: Syntax.TYPESCRIPT, content: this.serviceCode}
      ]
    },
    {
      title: 'MODULES.STYLES.THEMES.UPDATE', markdownList: [
        {title: 'src/app/app.component.ts', syntax: Syntax.TYPESCRIPT, content: this.updateCode}
      ]
    },
    {
      title: 'COMMON.LABELS.USAGE', markdownList: [
        {title: 'src/app/layout/navbar.component.ts', syntax: Syntax.TYPESCRIPT, content: this.usageTypescriptCode},
        {title: 'src/app/layout/navbar.component.html', syntax: Syntax.HTML, content: this.usageHTMLCode}
      ]
    },
  ];

  themes = THEMES_LIST;

  constructor(
    public themeService: ThemeService,
  ) {
  }

  changeTheme(theme: Theme): void {
    this.themeService.selectedTheme.next(theme);
  }

}
