import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { HomeComponent } from '@app/modules/home/components/home/home.component';
import { NotFoundComponent } from '@app/modules/home/components/not-found/not-found.component';
import { LoginComponent } from '@app/modules/home/components/login/login.component';
import { HomeRoutingModule } from '@app/modules/home/home-routing.module';

@NgModule({
  declarations: [
    HomeComponent,
    NotFoundComponent,
    LoginComponent
  ],
  imports: [
    HomeRoutingModule,
    SharedModule
  ]
})
export class HomeModule {
}
