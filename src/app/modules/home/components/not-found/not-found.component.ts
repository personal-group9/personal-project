import { Component } from '@angular/core';
import { ScrollbarService } from '@core/services/scrollbar.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent {

  constructor(
    public scrollbarService: ScrollbarService,
    public location: Location) {
  }

}
