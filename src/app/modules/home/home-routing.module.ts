import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UnauthenticatedGuard } from '@core/authentication/guards/unauthenticated-guard.service';
import { HomeComponent } from '@app/modules/home/components/home/home.component';
import { LoginComponent } from '@app/modules/home/components/login/login.component';

const homeRoutes: Routes = [
  {
    path: '',
    redirectTo: 'home'
  },
  {
    path: 'home',
    component: HomeComponent,
    data: {title: 'MODULES.COMMON.HOME.TITLE'}
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [UnauthenticatedGuard],
    data: {title: 'MODULES.COMMON.LOGIN.TITLE'}
  }
];

@NgModule({
  imports: [RouterModule.forChild(homeRoutes)],
  exports: [RouterModule]
})
export class HomeRoutingModule {
}
