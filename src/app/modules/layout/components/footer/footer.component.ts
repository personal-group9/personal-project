import { Component } from '@angular/core';
import { SetupItem } from '@shared/interfaces/setup-item';
import { Syntax } from '@shared/enums/syntaxs.enum';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent {

  htmlCode = `<div id="page-container">

  <header></header>

  <div>
    <router-outlet></router-outlet>
  </div>

  <footer id="footer"></footer>

</div>`;

  cssCode = `#page-container {
  position: relative;
  min-height: 100vh;
}

#footer {
  position: absolute;
  bottom: 0;
  width: 100%;
}`;

  setupStepList: SetupItem[] = [
    {
      title: 'COMMON.LABELS.USAGE', markdownList: [
        {title: 'src/app/app.component.html', syntax: Syntax.HTML, content: this.htmlCode},
        {title: 'src/app/app.component.css', syntax: Syntax.SCSS, content: this.cssCode},
      ]
    }
  ];

}
