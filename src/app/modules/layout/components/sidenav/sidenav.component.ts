import { Component } from '@angular/core';
import { SetupItem } from '@shared/interfaces/setup-item';
import { Syntax } from '@shared/enums/syntaxs.enum';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent {

  constantCode = `import { MenuItem } from '@core/interfaces/menu-item';

/**
 * In the first level of menu, avoid or not multi opened panels in sidenav.component.ts
 */
export const MULTI_EXPANSION_PANELS = false;

/**
 * How much padding you want to move in the left for each new level menu. in pixel.
 */
export const PADDING_LEFT_LEVEL_MENU_PX = 16;

export const AUTHENTICATED_MENU: MenuItem[] = [
  {
    title: 'TESTTT', icon: 'assignment', route: 'test', children: [
      {title: 'LOGIN1', icon: 'assignment', route: '404'}
      {title: 'LOGIN1', icon: 'assignment', route: 'test', children: [
          {title: 'LOGIN1', icon: 'assignment', route: '404'}
        ]
      },
      {icon: 'dashboard', route: 'home', title: 'LOGIN2'}
    ]
  },
  {
    title: 'TESTTT2', icon: 'assignment', route: 'test', children: [
      {
        title: 'LOGIN1', icon: 'assignment', route: 'test', children: [
          {title: 'LOGIN1', icon: 'assignment', route: '404'}
          {title: 'LOGIN2', icon: 'assignment', route: '404'}
        ]
      },
      {icon: 'dashboard', route: 'home', title: 'LOGIN2'}
    ]
  },
];

export const UNAUTHENTICATED_MENU: MenuItem[] = [
  {title: 'LAYOUT.SIDENAV.WELCOME', icon: 'home', route: 'home'},
  {
    title: 'LAYOUT.SIDENAV.ARCHITECTURES.TITLE', icon: 'architecture', route: 'architectures', children: [
      {
        title: 'LAYOUT.SIDENAV.ARCHITECTURES.SOFTWARE_ARCHITECTURE',
        icon: 'subdirectory_arrow_right',
        route: 'architectures/software-architecture',
        disabled: true
      },
      {
        title: 'LAYOUT.SIDENAV.ARCHITECTURES.FOLDERS_ORGANIZATION',
        icon: 'subdirectory_arrow_right',
        route: 'architectures/folders-organization'
      }
    ]
  },
  {
    title: 'LAYOUT.SIDENAV.CODE_ANALYSIS.TITLE', icon: 'graphic_eq', route: 'code-analysis', disabled: true, children: [
      {
        title: 'LAYOUT.SIDENAV.CODE_ANALYSIS.CODE_QUALITY',
        icon: 'subdirectory_arrow_right',
        route: 'code-analysis/code-quality',
        disabled: true
      },
      {
        title: 'LAYOUT.SIDENAV.CODE_ANALYSIS.CODE_SECURITY',
        icon: 'subdirectory_arrow_right',
        route: 'code-analysis/code-security',
        disabled: true
      }
    ]
  }
];`;

  modelCode = `export interface MenuItem {
  title: string;
  disabled?: boolean;
  icon?: string;
  route: string;
  children?: MenuItem[];
}`;

  tsMenuCode = `import { Component, OnInit, ViewChild } from '@angular/core';
import { SidenavService } from '@app/layout/sidenav/sidenav.service';
import { MULTI_EXPANSION_PANELS, AUTHENTICATED_MENU } from '@core/constants/menu.constant';
import { MenuItem } from '@core/interfaces/menu-item';
import { MatSidenav } from '@angular/material/sidenav';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {

  @ViewChild('sidenav', {static: true}) sidenav!: MatSidenav;
  menu = AUTHENTICATED_MENU;
  multiExpansionPanel = MULTI_EXPANSION_PANELS;

  constructor(
    private sidenavService: SidenavService
  ) {
  }

  ngOnInit(): void {
    this.sidenavService.setSidenav(this.sidenav);
  }

}`;

  htmlMenuCode = `<mat-sidenav-container>

  <mat-sidenav #sidenav mode="over">
    <perfect-scrollbar>
      <mat-nav-list>
        <mat-accordion [multi]="multiExpansionPanel">
          <app-sidenav-item *ngFor="let item of menu" [depth]="1" [item]="item"></app-sidenav-item>
        </mat-accordion>
      </mat-nav-list>
    </perfect-scrollbar>
  </mat-sidenav>

  <ng-content></ng-content>

</mat-sidenav-container>`;

  cssMenuCode = `.mat-nav-list {
  padding: 0;
}`;

  tsItemCode = `import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem } from '@core/interfaces/menu-item';
import { SidenavService } from '@app/layout/sidenav/sidenav.service';
import { PADDING_LEFT_LEVEL_MENU_PX } from '@core/constants/menu.constant';

@Component({
  selector: 'app-sidenav-item',
  templateUrl: './sidenav-item.component.html',
  styleUrls: ['./sidenav-item.component.scss'],
})
export class SidenavItemComponent {

  @Input() item!: MenuItem;
  @Input() depth!: number;
  itemPaddingLeftPx = PADDING_LEFT_LEVEL_MENU_PX;

  constructor(
    public router: Router,
    public sidenavService: SidenavService
  ) {
  }

}`;

  htmlItemCode = `<mat-expansion-panel [expanded]="router.isActive(this.item.route, false)" [hideToggle]="!item.children"
                     [disabled]="item.disabled" class="no-padding">

  <mat-expansion-panel-header (click)="!(item.children || item.disabled) ? sidenavService.close() : null"
                              [class]="router.isActive(this.item.route, false) ? 'active-item' : null"
                              [routerLink]="item.children || item.disabled ? null : item.route"
                              [style.padding-left.px]="depth*itemPaddingLeftPx" matRipple>
    <mat-panel-title class="align-items" style="color: inherit">
      <mat-icon class="me-2" mat-list-icon>{{ item.icon }}</mat-icon>
      {{ item.title | translate }}
    </mat-panel-title>
  </mat-expansion-panel-header>

  <app-sidenav-item *ngFor="let child of item.children" [depth]="depth+1" [item]="child"></app-sidenav-item>

</mat-expansion-panel>

<mat-divider *ngIf="depth==1"></mat-divider>`;

  cssItemCode = `.mat-expansion-panel {
  box-shadow: none !important;
  margin: 0 !important;
  border-radius: 0 !important;
}

.mat-expansion-panel-header {
  font-size: 14px !important;
  height: 48px !important;
}

.no-padding div .mat-expansion-panel-body {
  padding: 0 !important;
}`;

  tsServiceCode = `import { Injectable } from '@angular/core';
import { MatDrawerToggleResult, MatSidenav } from '@angular/material/sidenav';

@Injectable({
  providedIn: 'root'
})
export class SidenavService {

  private sidenav: MatSidenav;

  public setSidenav(sidenav: MatSidenav): void {
    this.sidenav = sidenav;
  }

  public open(): Promise<MatDrawerToggleResult> {
    return this.sidenav.open();
  }

  public close(): Promise<MatDrawerToggleResult> {
    return this.sidenav.close();
  }

  public toggle(isOpen?: boolean): Promise<MatDrawerToggleResult> {
    return this.sidenav.toggle(isOpen);
  }

}`;

  htmlCode = `<div>

  <app-navbar></app-navbar>

  <app-sidenav>

      <div class="container">
        <router-outlet></router-outlet>
      </div>

      <app-footer id="footer"></app-footer>

  </app-sidenav>

</div>`;

  setupStepList: SetupItem[] = [
    {
      title: 'MODULES.LAYOUT.SIDENAV.MODELS', markdownList: [
        {title: 'src/app/core/interfaces/menu-item.ts', syntax: Syntax.TYPESCRIPT, content: this.modelCode},
        {title: 'src/app/core/constants/menu.constant.ts', syntax: Syntax.TYPESCRIPT, content: this.constantCode}
      ]
    },
    {
      title: 'MODULES.LAYOUT.SIDENAV.MENU', markdownList: [
        {title: 'src/app/layout/sidenav/sidenav.component.ts', syntax: Syntax.TYPESCRIPT, content: this.tsMenuCode},
        {title: 'src/app/layout/sidenav/sidenav.component.html', syntax: Syntax.HTML, content: this.htmlMenuCode},
        {title: 'src/app/layout/sidenav/sidenav.component.scss', syntax: Syntax.SCSS, content: this.cssMenuCode}
      ]
    },
    {
      title: 'MODULES.LAYOUT.SIDENAV.ITEM', markdownList: [
        {title: 'src/app/layout/sidenav-item/sidenav-item.component.ts', syntax: Syntax.TYPESCRIPT, content: this.tsItemCode},
        {title: 'src/app/layout/sidenav-item/sidenav-item.component.html', syntax: Syntax.HTML, content: this.htmlItemCode},
        {title: 'src/app/layout/sidenav-item/sidenav-item.component.scss', syntax: Syntax.SCSS, content: this.cssItemCode}
      ]
    },
    {
      title: 'COMMON.LABELS.SERVICE', markdownList: [
        {title: 'src/app/core/services/sidenav.service.ts', syntax: Syntax.TYPESCRIPT, content: this.tsServiceCode}
      ]
    },
    {
      title: 'COMMON.LABELS.USAGE', markdownList: [
        {title: 'src/app/app.component.html', syntax: Syntax.HTML, content: this.htmlCode},
      ]
    },
  ];

}
