import { Component } from '@angular/core';
import { SetupItem } from '@shared/interfaces/setup-item';
import { Syntax } from '@shared/enums/syntaxs.enum';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {

  htmlCode = `<mat-toolbar color="primary">

  <button (click)="sidenavService.toggle()" class="max-height" mat-button>
    <mat-icon>menu</mat-icon>
  </button>

  <button (click)="sidenavService.close()" class="max-height" mat-button routerLink="/home">
    <span class="d-block d-sm-none">{{ 'PP' | translate }}</span>
    <span class="d-none d-sm-block">{{ 'TITLE' | translate }}</span>
  </button>

  <span class="spacer"></span>

  <button [matMenuTriggerFor]="themesMenu" class="max-height" mat-button>
    <span class="align-items">
      <mat-icon class="me-2">format_color_fill</mat-icon>
      <span class="d-none d-sm-block">{{ themeService.selectedTheme.getValue().name | translate }}</span>
      <mat-icon>arrow_drop_down</mat-icon>
    </span>
  </button>
  <mat-menu #themesMenu="matMenu" xPosition="before">
    <button (click)="changeTheme(theme)" *ngFor="let theme of themes"
            [class]="themeService.selectedTheme.getValue() === theme ? 'active-item' : null"
            mat-menu-item>
      <mat-icon *ngIf="theme.icon" [style.color]="theme.iconColor" class="me-2">{{ theme.icon }}</mat-icon>
      {{ theme.name | translate }}
    </button>
  </mat-menu>

  <button [matMenuTriggerFor]="languagesMenu" class="max-height" mat-button>
    <span class="align-items">
      <mat-icon class="me-2"
                svgIcon="{{ internationalizationService.selectedLanguage.getValue().icon?.name }}"></mat-icon>
      <span class="d-none d-sm-block">
        {{ 'LAYOUT.NAVBAR.LANGUAGES.' + internationalizationService.selectedLanguage.getValue().name | translate }}
      </span>
      <mat-icon>arrow_drop_down</mat-icon>
    </span>
  </button>
  <mat-menu #languagesMenu="matMenu" xPosition="before">
    <button (click)="changeLanguage(language)" *ngFor="let language of languages"
            [class]="internationalizationService.selectedLanguage.getValue() === language ? 'active-item' : null"
            mat-menu-item>
      <mat-icon class="me-2" svgIcon="{{ language.icon?.name }}"></mat-icon>
      {{ 'LAYOUT.NAVBAR.LANGUAGES.' + language.name | translate }}
    </button>
  </mat-menu>

  <button (click)="sidenavService.close()" *ngIf="!authenticationService.isLoggedIn"
          class="max-height" mat-button routerLink="/login">
    <mat-icon class="d-block d-sm-none">login</mat-icon>
    <span class="d-none d-sm-block">{{ 'MODULES.COMMON.LOGIN.LOGIN' | translate }}</span>
  </button>

  <button (click)="authenticationService.logout(); sidenavService.close()" *ngIf="authenticationService.isLoggedIn"
          class="max-height" mat-button>
    <mat-icon class="d-block d-sm-none">power_settings_new</mat-icon>
    <span class="d-none d-sm-block">{{ 'LAYOUT.NAVBAR.LOGOUT' | translate }}</span>
  </button>

</mat-toolbar>`;

  tsCode = `import { Component } from '@angular/core';
import { Languages } from '@core/constants/languages';
import { AuthenticationService } from '@core/authentication/services/authentication.service';
import { ThemeService } from '@core/services/theme.service';
import { SidenavService } from '@app/layout/sidenav/sidenav.service';
import { Language } from '@core/interfaces/language';
import { Themes } from '@core/constants/themes';
import { Theme } from '@core/interfaces/theme';
import { InternationalizationService } from '@core/services/internationalization.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {

  languages: Language[] = Languages.LANGUAGES_LIST;
  themes: Theme[] = Themes.THEMES_LIST;

  constructor(
    public internationalizationService: InternationalizationService,
    public authenticationService: AuthenticationService,
    public themeService: ThemeService,
    public sidenavService: SidenavService
  ) {
  }

  changeTheme(theme: Theme): void {
    this.themeService.selectedTheme.next(theme);
  }

  changeLanguage(language: Language): void {
    this.internationalizationService.selectedLanguage.next(language);
  }

}`;

  cssCode = `.mat-toolbar {
  position: relative;
  z-index: 2;
  box-shadow: 0 2px 4px -1px rgba(0, 0, 0, .2), 0 4px 5px 0 rgba(0, 0, 0, .14), 0 1px 10px 0 rgba(0, 0, 0, .12);
  padding-right: 0;
  padding-left: 0;
}

.max-height {
  height: 100%;
}`;

  setupStepList: SetupItem[] = [
    {
      title: 'COMMON.LABELS.USAGE', markdownList: [
        {title: 'src/app/layout/navbar/navbar.component.ts', syntax: Syntax.TYPESCRIPT, content: this.tsCode},
        {title: 'src/app/layout/navbar/navbar.component.html', syntax: Syntax.HTML, content: this.htmlCode},
        {title: 'src/app/layout/navbar/navbar.component.css', syntax: Syntax.SCSS, content: this.cssCode},
      ]
    }
  ];
}
