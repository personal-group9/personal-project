import { NgModule } from '@angular/core';
import { NavbarComponent } from '@app/modules/layout/components/navbar/navbar.component';
import { SidenavComponent } from '@app/modules/layout/components/sidenav/sidenav.component';
import { FooterComponent } from '@app/modules/layout/components/footer/footer.component';
import { SharedModule } from '@shared/shared.module';
import { LayoutRoutingModule } from '@app/modules/layout/layout-routing.module';

@NgModule({
  declarations: [
    NavbarComponent,
    SidenavComponent,
    FooterComponent
  ],
  imports: [
    LayoutRoutingModule,
    SharedModule
  ]
})
export class LayoutModule {
}
