import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NavbarComponent } from '@app/modules/layout/components/navbar/navbar.component';
import { SidenavComponent } from '@app/modules/layout/components/sidenav/sidenav.component';
import { FooterComponent } from '@app/modules/layout/components/footer/footer.component';
import { NotFoundComponent } from '@app/modules/home/components/not-found/not-found.component';

const layoutRoutes: Routes = [
  {
    path: 'navbar',
    component: NavbarComponent,
    data: {title: 'MODULES.LAYOUT.NAVBAR.TITLE'}
  },
  {
    path: 'sidenav',
    component: SidenavComponent,
    data: {title: 'MODULES.LAYOUT.SIDENAV.TITLE'}
  },
  {
    path: 'footer',
    component: FooterComponent,
    data: {title: 'MODULES.LAYOUT.FOOTER.TITLE'}
  },
  {
    path: '**',
    component: NotFoundComponent,
    data: {title: 'MODULES.COMMON.NOT_FOUND.TITLE'}
  }
];

@NgModule({
  imports: [RouterModule.forChild(layoutRoutes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule {
}
