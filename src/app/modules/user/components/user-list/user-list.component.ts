/* tslint:disable:max-line-length */
import { Component } from '@angular/core';
import { User } from '@app/modules/user/models/user';
import { ToastrService } from 'ngx-toastr';

const USERS_LIST: User[] = [
  { id: 1, createdBy: 'lexalis@gmail.com', createdDate: Date.now(), lastModifiedBy: 'lexalis@gmail.com', lastModifiedDate: Date.now(), firstname: 'Jean', lastname: 'Dupont', email: 'jean.dupont@hotmail.fr', mobilePhoneNumber: '06805696', fixPhoneNumber: '04509237', birthDate: new Date(), description: 'test', checked: true },
  { id: 2, createdBy: 'lexalis@gmail.com', createdDate: Date.now(), lastModifiedBy: 'lexalis@gmail.com', lastModifiedDate: Date.now(), firstname: 'Alex', lastname: 'Dupont', email: 'jean.dupont@hotmail.fr', mobilePhoneNumber: '06805696', fixPhoneNumber: '04509237', birthDate: new Date(), description: 'test', checked: true },
  { id: 3, createdBy: 'lexalis@gmail.com', createdDate: Date.now(), lastModifiedBy: 'lexalis@gmail.com', lastModifiedDate: Date.now(), firstname: 'Bob', lastname: 'Dupont', email: 'jean.dupont@hotmail.fr', mobilePhoneNumber: '06805696', fixPhoneNumber: '04509237', birthDate: new Date(), description: 'test', checked: true },
  { id: 4, createdBy: 'lexalis@gmail.com', createdDate: Date.now(), lastModifiedBy: 'lexalis@gmail.com', lastModifiedDate: Date.now(), firstname: 'Franc', lastname: 'Dupont', email: 'jean.dupont@hotmail.fr', mobilePhoneNumber: '06805696', fixPhoneNumber: '04509237', birthDate: new Date(), description: 'test', checked: true },
  { id: 5, createdBy: 'lexalis@gmail.com', createdDate: Date.now(), lastModifiedBy: 'lexalis@gmail.com', lastModifiedDate: Date.now(), firstname: 'Lucie', lastname: 'Dupont', email: 'jean.dupont@hotmail.fr', mobilePhoneNumber: '06805696', fixPhoneNumber: '04509237', birthDate: new Date(), description: 'test', checked: true },
  { id: 6, createdBy: 'lexalis@gmail.com', createdDate: Date.now(), lastModifiedBy: 'lexalis@gmail.com', lastModifiedDate: Date.now(), firstname: 'Alle', lastname: 'Dupont', email: 'jean.dupont@hotmail.fr', mobilePhoneNumber: '06805696', fixPhoneNumber: '04509237', birthDate: new Date(), description: 'test', checked: true },
  { id: 7, createdBy: 'lexalis@gmail.com', createdDate: Date.now(), lastModifiedBy: 'lexalis@gmail.com', lastModifiedDate: Date.now(), firstname: 'Jean', lastname: 'Dupont', email: 'jean.dupont@hotmail.fr', mobilePhoneNumber: '06805696', fixPhoneNumber: '04509237', birthDate: new Date(), description: 'test', checked: true },
  { id: 8, createdBy: 'lexalis@gmail.com', createdDate: Date.now(), lastModifiedBy: 'lexalis@gmail.com', lastModifiedDate: Date.now(), firstname: 'Jean', lastname: 'Dupont', email: 'jean.dupont@hotmail.fr', mobilePhoneNumber: '06805696', fixPhoneNumber: '04509237', birthDate: new Date(), description: 'test', checked: true },
  { id: 9, createdBy: 'lexalis@gmail.com', createdDate: Date.now(), lastModifiedBy: 'lexalis@gmail.com', lastModifiedDate: Date.now(), firstname: 'Jean', lastname: 'Dupont', email: 'jean.dupont@hotmail.fr', mobilePhoneNumber: '06805696', fixPhoneNumber: '04509237', birthDate: new Date(), description: 'test', checked: true },
  { id: 10, createdBy: 'lexalis@gmail.com', createdDate: Date.now(), lastModifiedBy: 'lexalis@gmail.com', lastModifiedDate: Date.now(), firstname: 'Jean', lastname: 'Dupont', email: 'jean.dupont@hotmail.fr', mobilePhoneNumber: '06805696', fixPhoneNumber: '04509237', birthDate: new Date(), description: 'test', checked: true },
  { id: 11, createdBy: 'lexalis@gmail.com', createdDate: Date.now(), lastModifiedBy: 'lexalis@gmail.com', lastModifiedDate: Date.now(), firstname: 'Jean', lastname: 'Dupont', email: 'jean.dupont@hotmail.fr', mobilePhoneNumber: '06805696', fixPhoneNumber: '04509237', birthDate: new Date(), description: 'test', checked: true },
  { id: 12, createdBy: 'lexalis@gmail.com', createdDate: Date.now(), lastModifiedBy: 'lexalis@gmail.com', lastModifiedDate: Date.now(), firstname: 'Jean', lastname: 'Dupont', email: 'jean.dupont@hotmail.fr', mobilePhoneNumber: '06805696', fixPhoneNumber: '04509237', birthDate: new Date(), description: 'test', checked: true },
  { id: 13, createdBy: 'lexalis@gmail.com', createdDate: Date.now(), lastModifiedBy: 'lexalis@gmail.com', lastModifiedDate: Date.now(), firstname: 'Jean', lastname: 'Dupont', email: 'jean.dupont@hotmail.fr', mobilePhoneNumber: '06805696', fixPhoneNumber: '04509237', birthDate: new Date(), description: 'test', checked: true },
];

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent {

  displayedColumns: string[] = ['id', 'firstname', 'lastname', 'email', 'mobilePhoneNumber', 'edit'];
  dataSource = USERS_LIST;

  constructor(private toastrService: ToastrService) {}

  remove(id: number): void {
    this.dataSource = [...this.dataSource.filter(x => x.id !== id)];
    this.toastrService.success('L\'utilisateur n°' + id + ' a été supprimé');
  }

}
