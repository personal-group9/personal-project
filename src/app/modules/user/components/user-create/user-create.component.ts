import { Component } from '@angular/core';
import { User } from '@app/modules/user/models/user';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.scss']
})
export class UserCreateComponent {

  model: User;

  constructor(private activatedRoute: ActivatedRoute) {
    this.model = new User({});

    this.activatedRoute.url.subscribe(fragment => {
      if (fragment[0].path !== 'new') {
        this.model = new User({
          id: 2,
          createdBy: 'lexalis@gmail.com',
          createdDate: Date.now(),
          lastModifiedBy: 'lexalis@gmail.com',
          lastModifiedDate: Date.now(),
          firstname: 'Jean',
          lastname: 'Dupont',
          email: 'jean.dupont@hotmail.fr',
          mobilePhoneNumber: '06805696',
          fixPhoneNumber: '04509237',
          birthDate: new Date(),
          description: 'test',
          checked: true
        });
      }
    });
  }

}
