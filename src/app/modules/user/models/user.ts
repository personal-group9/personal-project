import ApiModel from '@core/api/api-model';

export class User extends ApiModel<User> {
  public firstname?: string;
  public lastname?: string;
  public email?: string;
  public mobilePhoneNumber?: string;
  public fixPhoneNumber?: string;
  public birthDate?: Date;
  public description?: string;
  public checked?: boolean;

  constructor(data: any) {
    super('localhost:4200', '/user', data);
  }
}
