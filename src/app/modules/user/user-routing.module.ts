import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserListComponent } from '@app/modules/user/components/user-list/user-list.component';
import { NotFoundComponent } from '@app/modules/home/components/not-found/not-found.component';
import { UserCreateComponent } from '@app/modules/user/components/user-create/user-create.component';

const userRoutes: Routes = [
  {
    path: '',
    component: UserListComponent
  },
  {
    path: ':userId',
    component: UserCreateComponent
  },
  {
    path: '**',
    component: NotFoundComponent,
    data: {title: 'MODULES.COMMON.NOT_FOUND.TITLE'}
  }
];

@NgModule({
  imports: [RouterModule.forChild(userRoutes)],
  exports: [RouterModule]
})
export class UserRoutingModule {
}
