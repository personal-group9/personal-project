import { NgModule } from '@angular/core';
import { DevopsRoutingModule } from '@app/modules/devops/devops-routing.module';
import { SharedModule } from '@shared/shared.module';
import { ContinuousIntegrationComponent } from '@app/modules/devops/components/continuous-integration/continuous-integration.component';
import { ContinuousDeliveryComponent } from '@app/modules/devops/components/continuous-delivery/continuous-delivery.component';
import { ContinuousDeploymentComponent } from '@app/modules/devops/components/continuous-deployment/continuous-deployment.component';

@NgModule({
  declarations: [
    ContinuousIntegrationComponent,
    ContinuousDeliveryComponent,
    ContinuousDeploymentComponent
  ],
  imports: [
    DevopsRoutingModule,
    SharedModule
  ]
})
export class DevopsModule {
}
