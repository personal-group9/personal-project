import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContinuousIntegrationComponent } from '@app/modules/devops/components/continuous-integration/continuous-integration.component';
import { ContinuousDeliveryComponent } from '@app/modules/devops/components/continuous-delivery/continuous-delivery.component';
import { ContinuousDeploymentComponent } from '@app/modules/devops/components/continuous-deployment/continuous-deployment.component';
import { NotFoundComponent } from '@app/modules/home/components/not-found/not-found.component';
import {PipelineDiagramComponent} from '@app/modules/architectures/components/pipeline-diagram/pipeline-diagram.component';

const devopsRoutes: Routes = [
  {
    path: 'continuous-integration',
    component: ContinuousIntegrationComponent,
    data: {title: 'MODULES.DEVOPS.CONTINUOUS_INTEGRATION.TITLE'}
  },
  {
    path: 'continuous-delivery',
    component: ContinuousDeliveryComponent,
    data: {title: 'MODULES.DEVOPS.CONTINUOUS_DELIVERY.TITLE'}
  },
  {
    path: 'continuous-deployment',
    component: ContinuousDeploymentComponent,
    data: {title: 'MODULES.DEVOPS.CONTINUOUS_DEPLOYMENT.TITLE'}
  },
  {
    path: 'pipeline-diagram',
    component: PipelineDiagramComponent,
    data: {title: 'MODULES.ARCHITECTURES.PIPELINE_DIAGRAM.TITLE'}
  },
  {
    path: '**',
    component: NotFoundComponent,
    data: {title: 'MODULES.COMMON.NOT_FOUND.TITLE'}
  }
];

@NgModule({
  imports: [RouterModule.forChild(devopsRoutes)],
  exports: [RouterModule]
})
export class DevopsRoutingModule {
}
