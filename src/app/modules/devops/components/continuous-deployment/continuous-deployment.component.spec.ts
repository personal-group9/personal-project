import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContinuousDeploymentComponent } from './continuous-deployment.component';
import { CoreModule } from '@core/core.module';
import { SharedModule } from '@shared/shared.module';
import { RouterTestingModule } from '@angular/router/testing';

describe('ContinuousDeploymentComponent', () => {
  let component: ContinuousDeploymentComponent;
  let fixture: ComponentFixture<ContinuousDeploymentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CoreModule, SharedModule, RouterTestingModule],
      declarations: [ContinuousDeploymentComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContinuousDeploymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
