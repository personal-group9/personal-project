import { Component } from '@angular/core';
import { SetupItem } from '@shared/interfaces/setup-item';
import { Syntax } from '@shared/enums/syntaxs.enum';

@Component({
  selector: 'app-continuous-deployment',
  templateUrl: './continuous-deployment.component.html',
  styleUrls: ['./continuous-deployment.component.scss']
})
export class ContinuousDeploymentComponent {

  gitCode = `image: node:latest

cache:
  paths:
    - node_modules/

stages:
  - build
  - deploy

build-job:
  stage: build
  only:
    - merge_requests
    - master
  artifacts:
    paths:
      - dist/
  script:
    - npm install
    - npm run build

firebase-deploy-job:
  stage: deploy
  only:
    - master
  dependencies:
    - build-job
  script:
    - npm install
    - npm run firebase-staging-environment
    - npm run firebase-deploy
  environment:
    name: staging
    url: https://$FIREBASE_APP_STAGING.web.app/

heroku-deploy-job:
  stage: deploy
  only:
    - master
  image: ruby:latest
  script:
    - apt-get update -qy
    - apt-get install -y ruby-dev
    - gem install dpl
    - dpl --provider=heroku --app=$HEROKU_APP_STAGING --api-key=$HEROKU_API_KEY
  environment:
    name: staging
    url: https://$HEROKU_APP_STAGING.herokuapp.com`;

  herokyPackage = `{
  "name": "personal-project",
  "version": "0.0.0",
  "scripts": {
    "ng": "ng",
    "start": "node server.js",
    "local-start": "ng serve",
    "build": "ng build",
    "test": "ng test",
    "lint": "ng lint",
    "e2e": "ng e2e",
    "firebase-staging-environment": "firebase use --token $FIREBASE_TOKEN staging",
    "firebase-deploy": "firebase deploy --token $FIREBASE_TOKEN --non-interactive"
  },
  "private": true,
  "dependencies": {
    "@angular/animations": "~11.0.9",
    "@angular/cdk": "^11.1.0",
    "@angular/cli": "~11.0.7",
    "@angular/common": "~11.0.9",
    "@angular/compiler": "~11.0.9",
    "@angular/compiler-cli": "~11.0.9",
    "@angular/core": "~11.0.9",
    "@angular/fire": "^6.1.4",
    "@angular/forms": "~11.0.9",
    "@angular/material": "^11.1.0",
    "@angular/platform-browser": "~11.0.9",
    "@angular/platform-browser-dynamic": "~11.0.9",
    "@angular/router": "~11.0.9",
    "@ngx-translate/core": "^13.0.0",
    "@ngx-translate/http-loader": "^6.0.0",
    "express": "^4.17.1",
    "rxjs": "~6.6.0",
    "tslib": "^2.0.0",
    "typescript": "~4.0.2",
    "zone.js": "~0.10.2"
  },
  "devDependencies": {
    "@angular-devkit/build-angular": "~0.1100.7",
    "@types/jasmine": "^3.6.3",
    "@types/node": "^12.19.15",
    "codelyzer": "^6.0.0",
    "firebase-tools": "^9.2.2",
    "jasmine-core": "~3.6.0",
    "jasmine-spec-reporter": "~5.0.0",
    "karma": "~5.1.0",
    "karma-chrome-launcher": "~3.1.0",
    "karma-coverage": "~2.0.3",
    "karma-jasmine": "~4.0.0",
    "karma-jasmine-html-reporter": "^1.5.0",
    "protractor": "~7.0.0",
    "ts-node": "~8.3.0",
    "tslint": "~6.1.0"
  },
  "engines": {
    "node": "v15.9.0",
    "npm": "7.5.3"
  }
}`;

  expressCode = `//Install express server
const express = require('express');
const path = require('path');

const app = express();

// Serve only the static files form the dist directory
app.use(express.static(__dirname + '/dist/personal-project'));

app.get('/*', function (req, res) {

  res.sendFile(path.join(__dirname + '/dist/personal-project/index.html'));
});

// Start the app by listening on the default Heroku port
app.listen(process.env.PORT || 8080);
`;

  fireCode = `{
  "hosting": {
    "public": "dist/personal-project",
    "ignore": [
      "firebase.json",
      "**/.*",
      "**/node_modules/**"
    ],
    "rewrites": [
      {
        "source": "**",
        "destination": "/index.html"
      }
    ]
  }
}`;

  setupHerokuList: SetupItem[] = [
    {
      title: 'COMMON.LABELS.DEPENDENCY', markdownList: [
        {title: 'Terminal', syntax: Syntax.BASH, content: 'npm install express --save'},
      ]
    },
    {
      title: 'MODULES.DEVOPS.CONTINUOUS_DEPLOYMENT.EXPRESS_PARAMETERS', markdownList: [
        {title: 'server.js', syntax: Syntax.JAVASCRIPT, content: this.expressCode},
      ]
    },
    {
      title: 'Package.json', description: 'MODULES.DEVOPS.CONTINUOUS_DEPLOYMENT.PACKAGE', markdownList: [
        {title: 'package.json', syntax: Syntax.JSON, content: this.herokyPackage},
      ]
    },
    {title: 'COMMON.LABELS.PIPELINE_VARIABLES', description: 'MODULES.DEVOPS.CONTINUOUS_DEPLOYMENT.HEROKU_VARIABLES'},
  ];

  setupFirebaseList: SetupItem[] = [
    {
      title: 'COMMON.LABELS.DEPENDENCY', markdownList: [
        {title: 'Terminal', syntax: Syntax.BASH, content: 'npm install firebase-tools --save-dev'},
      ]
    },
    {
      title: 'MODULES.DEVOPS.CONTINUOUS_DEPLOYMENT.FIREBASE_PARAMETERS', markdownList: [
        {title: 'firebase.json', syntax: Syntax.JSON, content: this.fireCode},
        {
          title: '.firebaserc', syntax: Syntax.JSON, content: '{\n' +
            '  "projects": {\n' +
            '    "staging": "personal-project-firebase-hosting-staging",\n' +
            '    "production": "personal-project-firebase-hosting-production"\n' +
            '  }\n' +
            '}\n'
        }
      ]
    },
    {title: 'COMMON.LABELS.PIPELINE_VARIABLES', description: 'MODULES.DEVOPS.CONTINUOUS_DEPLOYMENT.FIREBASE_VARIABLES'}
  ];

  setupGitLabList: SetupItem[] = [
    {
      title: 'MODULES.DEVOPS.CONTINUOUS_INTEGRATION.GIT', markdownList: [
        {title: '.gitlab-ci.yml', syntax: Syntax.YAML, content: this.gitCode}
      ]
    }
  ];

}
