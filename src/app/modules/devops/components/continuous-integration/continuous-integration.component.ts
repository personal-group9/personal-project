import { Component } from '@angular/core';
import { SetupItem } from '@shared/interfaces/setup-item';
import { Syntax } from '@shared/enums/syntaxs.enum';

@Component({
  selector: 'app-continuous-integration',
  templateUrl: './continuous-integration.component.html',
  styleUrls: ['./continuous-integration.component.scss']
})
export class ContinuousIntegrationComponent {

  gitCode = `image: node:latest

cache:
  paths:
    - node_modules/

stages:
  - build
  - test
  - code-analysis

build-job:
  stage: build
    only:
    - merge_requests
    - master
  script:
    - npm install
    - npm run build

unit-tests-job:
  stage: test
    only:
    - merge_requests
    - master
  artifacts:
    paths:
      - coverage/
  script:
    - npm install
    # install dependencies to use chrome puppeteer
    - apt update && apt install -yq gconf-service libasound2 libatk1.0-0 libc6 libcairo2 libcups2 libdbus-1-3 libexpat1 libfontconfig1 libgcc1 libgconf-2-4 libgdk-pixbuf2.0-0 libglib2.0-0 libgtk-3-0 libnspr4 libpango-1.0-0 libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 libxcomposite1 libxcursor1 libxdamage1 libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxss1 libxtst6 ca-certificates fonts-liberation libappindicator1 libnss3 lsb-release xdg-utils wget
    - npm run test-ci

e2e-tests-job:
  stage: test
    only:
    - merge_requests
    - master
  script:
    - npm install
    # install dependencies to use chrome puppeteer
    - apt update && apt install -yq gconf-service libasound2 libatk1.0-0 libc6 libcairo2 libcups2 libdbus-1-3 libexpat1 libfontconfig1 libgcc1 libgconf-2-4 libgdk-pixbuf2.0-0 libglib2.0-0 libgtk-3-0 libnspr4 libpango-1.0-0 libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 libxcomposite1 libxcursor1 libxdamage1 libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxss1 libxtst6 ca-certificates fonts-liberation libappindicator1 libnss3 lsb-release xdg-utils wget
    - npm run e2e

lint-analysis-job:
  stage: code-analysis
  only:
    - merge_requests
    - master
  script:
    - npm install
    - npm run lint

sonar-analysis-job:
  stage: code-analysis
  only:
    - merge_requests
    - master
  dependencies:
    - unit-tests-job
  script: npm run sonar`;

  protractorCode = `// @ts-check
// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const { SpecReporter, StacktraceOption } = require('jasmine-spec-reporter');

/**
 * @type { import("protractor").Config }
 */
exports.config = {
  allScriptsTimeout: 11000,
  specs: [
    './src/**/*.e2e-spec.ts'
  ],
  capabilities: {
    browserName: 'chrome',
    chromeOptions: {
      binary: require('puppeteer').executablePath(),
      args: ['--headless', '--no-sandbox']
    }
  },
  directConnect: true,
  SELENIUM_PROMISE_MANAGER: false,
  baseUrl: 'http://localhost:4200/',
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    print: function() {}
  },
  onPrepare() {
    require('ts-node').register({
      project: require('path').join(__dirname, './tsconfig.json')
    });
    jasmine.getEnv().addReporter(new SpecReporter({
      spec: {
        displayStacktrace: StacktraceOption.PRETTY
      }
    }));
  }
};`;

  karmaCode = `// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

const puppeteer = require('puppeteer');
process.env.CHROME_BIN = puppeteer.executablePath();

module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine', '@angular-devkit/build-angular'],
    plugins: [
      require('karma-jasmine'),
      require('karma-chrome-launcher'),
      require('karma-jasmine-html-reporter'),
      require('karma-coverage'),
      require('@angular-devkit/build-angular/plugins/karma')
    ],
    client: {
      jasmine: {
        // you can add configuration options for Jasmine here
        // the possible options are listed at https://jasmine.github.io/api/edge/Configuration.html
        // for example, you can disable the random execution with \`random: false\`
        // or set a specific seed with \`seed: 4321\`
      },
      clearContext: false // leave Jasmine Spec Runner output visible in browser
    },
    jasmineHtmlReporter: {
      suppressAll: true // removes the duplicated traces
    },
    coverageReporter: {
      dir: require('path').join(__dirname, './coverage/personal-project'),
      subdir: '.',
      reporters: [
        {type: 'html'},
        {type: 'text-summary'},
        {type: 'lcovonly'}
      ]
    },
    reporters: ['progress', 'kjhtml'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['Chrome'],
    customLaunchers: {
      ChromeHeadlessNoSandbox: {
        base: 'ChromeHeadless',
        flags: ['--no-sandbox']
      }
    },
    singleRun: false,
    restartOnFileChange: true
  });
};`;

  setupStepList: SetupItem[] = [
    {
      title: 'MODULES.TESTING.UNIT_TESTING.TITLE', markdownList: [
        {title: 'Terminal', syntax: Syntax.BASH, content: 'npm install puppeteer --save-dev'},
        {
          title: 'package.json', syntax: Syntax.JSON, content: '{\n' +
            '  "name": "personal-project",\n' +
            '  "version": "0.0.0",\n' +
            '  "scripts": {\n' +
            '    "ng": "ng",\n' +
            '    "start": "ng serve",\n' +
            '    "build": "ng build",\n' +
            '    "test": "ng test",\n' +
            '    "lint": "ng lint",\n' +
            '    "e2e": "ng e2e",\n' +
            '    "test-ci": "ng test --no-watch --no-progress --code-coverage --browsers=ChromeHeadlessNoSandbox",'
        },
        {title: 'karma.conf.js', syntax: Syntax.JAVASCRIPT, content: this.karmaCode}
      ]
    },
    {
      title: 'MODULES.TESTING.END_TO_END_TESTING.TITLE', markdownList: [
        {title: 'e2e/src/protractor.conf.js', syntax: Syntax.JAVASCRIPT, content: this.protractorCode}
      ]
    },
    {
      title: 'COMMON.LABELS.PIPELINE_VARIABLES', description: 'MODULES.DEVOPS.CONTINUOUS_INTEGRATION.SONAR'
    },
    {
      title: 'MODULES.DEVOPS.CONTINUOUS_INTEGRATION.GIT', markdownList: [
        {title: '.gitlab-ci.yml', syntax: Syntax.YAML, content: this.gitCode}
      ]
    }
  ];

}
