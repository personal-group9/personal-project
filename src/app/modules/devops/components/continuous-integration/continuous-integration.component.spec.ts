import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContinuousIntegrationComponent } from './continuous-integration.component';
import { CoreModule } from '@core/core.module';
import { SharedModule } from '@shared/shared.module';
import { RouterTestingModule } from '@angular/router/testing';

describe('ContinuousIntegrationComponent', () => {
  let component: ContinuousIntegrationComponent;
  let fixture: ComponentFixture<ContinuousIntegrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CoreModule, SharedModule, RouterTestingModule],
      declarations: [ContinuousIntegrationComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContinuousIntegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
