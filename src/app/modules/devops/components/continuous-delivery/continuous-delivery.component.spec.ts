import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContinuousDeliveryComponent } from './continuous-delivery.component';
import { CoreModule } from '@core/core.module';
import { SharedModule } from '@shared/shared.module';
import { RouterTestingModule } from '@angular/router/testing';

describe('ContinuousDeliveryComponent', () => {
  let component: ContinuousDeliveryComponent;
  let fixture: ComponentFixture<ContinuousDeliveryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CoreModule, SharedModule, RouterTestingModule],
      declarations: [ContinuousDeliveryComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContinuousDeliveryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
