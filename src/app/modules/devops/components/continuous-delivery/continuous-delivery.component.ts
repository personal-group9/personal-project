import { Component } from '@angular/core';
import { SetupItem } from '@shared/interfaces/setup-item';
import { Syntax } from '@shared/enums/syntaxs.enum';

@Component({
  selector: 'app-continuous-delivery',
  templateUrl: './continuous-delivery.component.html',
  styleUrls: ['./continuous-delivery.component.scss']
})
export class ContinuousDeliveryComponent {

  gitCode = `image: node:latest

cache:
  paths:
    - node_modules/

stages:
  - build
  - delivery

build-job:
  stage: build
  only:
    - merge_requests
    - master
  artifacts:
    paths:
      - dist/
  script:
    - npm install
    - npm run build

artifact-delivery-job:
  stage: delivery
  only:
    - master
  dependencies:
    - build-job
  script:
    - export VERSION=$(node -p "require('./package.json').version")
    - tar -czvf personal-project-$VERSION.tgz dist/personal-project
    - curl -u $ARTIFACTORY_USER:$ARTIFACTORY_TOKEN -X PUT $ARTIFACTORY_URL -T personal-project-$VERSION.tgz
  environment:
    name: staging
    url: https://personalproject.jfrog.io/artifactory/`;

  setupStepList: SetupItem[] = [
    {
      title: 'MODULES.DEVOPS.CONTINUOUS_INTEGRATION.GIT', markdownList: [
        {title: '.gitlab-ci.yml', syntax: Syntax.YAML, content: this.gitCode}
      ]
    }
  ];
}
