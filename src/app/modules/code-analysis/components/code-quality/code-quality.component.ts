import { Component } from '@angular/core';
import { SetupItem } from '@shared/interfaces/setup-item';
import { Syntax } from '@shared/enums/syntaxs.enum';

@Component({
  selector: 'app-code-quality',
  templateUrl: './code-quality.component.html',
  styleUrls: ['./code-quality.component.scss']
})
export class CodeQualityComponent {

  sonarCode = `sonar.projectKey=personal-group9_personal-project
sonar.organization=personal-group9
# This is the name and version displayed in the SonarCloud UI.
sonar.projectName=Personal Project
sonar.projectVersion=1.0
# Path is relative to the sonar-project.properties file. Replace "\\" by "/" on Windows.
sonar.sources=src
sonar.tests=src
# Encoding of the source code. Default is default system encoding
sonar.sourceEncoding=UTF-8
# TypeScript specific config
sonar.typescript.lcov.reportPaths=coverage/personal-project/lcov.info
sonar.test.inclusions=**/*.spec.ts
sonar.exclusions=**/node_modules/**, **/src/environments**, **/src/main.ts
`;

  packageCode = `{
  "name": "personal-project",
  "version": "0.0.0",
  "scripts": {
    "ng": "ng",
    "start": "ng serve",
    "build": "ng build",
    "test": "ng test",
    "lint": "ng lint",
    "e2e": "ng e2e",
    "code-coverage": "ng test --no-watch --no-progress --code-coverage",
    "sonar": "sonar-scanner"
  },
  ...`;

  setupStepList: SetupItem[] = [
    {
      title: 'COMMON.LABELS.DEPENDENCY', markdownList: [
        {title: 'Terminal', syntax: Syntax.BASH, content: 'npm install sonarqube-scanner --save'}
      ]
    },
    {
      title: 'MODULES.CODE_ANALYSIS.CODE_QUALITY.SONAR', markdownList: [
        {title: 'sonar-project.properties', syntax: Syntax.PROPERTIES, content: this.sonarCode}
      ]
    },
    {
      title: 'MODULES.CODE_ANALYSIS.CODE_QUALITY.COVERAGE', markdownList: [
        {title: 'package.json', syntax: Syntax.JSON, content: this.packageCode}
      ]
    },
    {
      title: 'COMMON.LABELS.USAGE', description: 'MODULES.CODE_ANALYSIS.CODE_QUALITY.TOKEN', markdownList: [
        {title: 'Terminal', syntax: Syntax.BASH, content: 'npm run code-coverage'},
        {title: 'Terminal', syntax: Syntax.BASH, content: 'sonar-scanner -Dsonar.login=XXXX -Dsonar.host.url=https://sonarcloud.io'}
      ]
    }
  ];

}
