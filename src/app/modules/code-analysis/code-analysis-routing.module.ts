import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CodeQualityComponent } from '@app/modules/code-analysis/components/code-quality/code-quality.component';
import { CodeSecurityComponent } from '@app/modules/code-analysis/components/code-security/code-security.component';
import { NotFoundComponent } from '@app/modules/home/components/not-found/not-found.component';

const codeAnalysisRoutes: Routes = [
  {
    path: 'code-quality',
    component: CodeQualityComponent,
    data: {title: 'MODULES.CODE_ANALYSIS.CODE_QUALITY.TITLE'}
  },
  {
    path: 'code-security',
    component: CodeSecurityComponent,
    data: {title: 'MODULES.CODE_ANALYSIS.CODE_QUALITY.TITLE'}
  },
  {
    path: '**',
    component: NotFoundComponent,
    data: {title: 'MODULES.COMMON.NOT_FOUND.TITLE'}
  }
];

@NgModule({
  imports: [RouterModule.forChild(codeAnalysisRoutes)],
  exports: [RouterModule]
})
export class CodeAnalysisRoutingModule {
}
