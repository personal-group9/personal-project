import { NgModule } from '@angular/core';
import { CodeQualityComponent } from './components/code-quality/code-quality.component';
import { CodeSecurityComponent } from './components/code-security/code-security.component';
import { SharedModule } from '@shared/shared.module';
import { CodeAnalysisRoutingModule } from '@app/modules/code-analysis/code-analysis-routing.module';

@NgModule({
  declarations: [
    CodeQualityComponent,
    CodeSecurityComponent
  ],
  imports: [
    CodeAnalysisRoutingModule,
    SharedModule
  ]
})
export class CodeAnalysisModule {
}
