import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FoldersOrganizationComponent } from '@app/modules/architectures/components/folders-organization/folders-organization.component';
import { SoftwareArchitectureComponent } from '@app/modules/architectures/components/software-architecture/software-architecture.component';
import { NotFoundComponent } from '@app/modules/home/components/not-found/not-found.component';
import {PipelineDiagramComponent} from '@app/modules/architectures/components/pipeline-diagram/pipeline-diagram.component';

const architecturesRoutes: Routes = [
  {
    path: 'folders-organization',
    component: FoldersOrganizationComponent,
    data: {title: 'MODULES.ARCHITECTURES.FOLDERS_ORGANIZATION.TITLE'}
  },
  {
    path: 'software-architecture',
    component: SoftwareArchitectureComponent,
    data: {title: 'MODULES.ARCHITECTURES.SOFTWARE_ARCHITECTURE.TITLE'}
  },
  {
    path: 'pipeline-diagram',
    component: PipelineDiagramComponent,
    data: {title: 'MODULES.ARCHITECTURES.PIPELINE_DIAGRAM.TITLE'}
  },
  {
    path: '**',
    component: NotFoundComponent,
    data: {title: 'MODULES.COMMON.NOT_FOUND.TITLE'}
  }
];

@NgModule({
  imports: [RouterModule.forChild(architecturesRoutes)],
  exports: [RouterModule]
})
export class ArchitecturesRoutingModule {
}
