import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { ArchitecturesRoutingModule } from '@app/modules/architectures/architectures-routing.module';
import { SoftwareArchitectureComponent } from '@app/modules/architectures/components/software-architecture/software-architecture.component';
import { FoldersOrganizationComponent } from '@app/modules/architectures/components/folders-organization/folders-organization.component';
import { PipelineDiagramComponent } from './components/pipeline-diagram/pipeline-diagram.component';

@NgModule({
  declarations: [
    SoftwareArchitectureComponent,
    FoldersOrganizationComponent,
    PipelineDiagramComponent
  ],
  imports: [
    ArchitecturesRoutingModule,
    SharedModule
  ]
})
export class ArchitecturesModule {
}
