import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SoftwareArchitectureComponent } from './software-architecture.component';
import { CoreModule } from '@core/core.module';
import { SharedModule } from '@shared/shared.module';
import { RouterTestingModule } from '@angular/router/testing';

describe('SoftwareArchitectureComponent', () => {
  let component: SoftwareArchitectureComponent;
  let fixture: ComponentFixture<SoftwareArchitectureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CoreModule, SharedModule, RouterTestingModule],
      declarations: [SoftwareArchitectureComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SoftwareArchitectureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
