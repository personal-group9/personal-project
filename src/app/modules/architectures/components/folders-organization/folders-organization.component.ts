import { Component } from '@angular/core';
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource } from '@angular/material/tree';

interface FolderNode {
  name: string;
  description?: string;
  file?: boolean;
  children?: FolderNode[];
}

const TREE_DATA: FolderNode[] = [
  {
    name: 'src/', description: 'Default code source folder for Angular', children: [
      {
        name: 'app/', children: [
          {
            name: 'core/', description: 'Contains code to instantiate your app and load some core functionality', children: [
              {
                name: 'authentication/', description: 'Default code source folder for Angular', children: [
                  {
                    name: 'guards/', description: 'Protect routes access', children: [
                      {name: 'authenticated-guard.service.ts', description: 'Protect routes against unauthenticated users', file: true},
                      {name: 'unauthenticated-guard.service.ts', description: 'Protect routes against authenticated users', file: true},
                    ]
                  },
                  {
                    name: 'services/', children: [
                      {name: 'authentication.service.ts', file: true}
                    ]
                  },
                ]
              },
              {
                name: 'constants/', children: [
                  {name: 'language.constant.ts', file: true}
                ]
              },
              {
                name: 'errors/', description: 'Handle differents types of errors', children: [
                  {name: 'global-error-handler.ts', file: true},
                  {name: 'http-error.interceptor.ts', file: true}
                ]
              },
              {name: 'interfaces/', children: [
                  {name: 'language.ts', file: true}
                ]
              },
              {name: 'models/'},
              {
                name: 'services/', children: [
                  {name: 'theme.service.ts', file: true}
                ]
              },
              {name: 'core.module.ts', file: true}
            ]
          },
          {
            name: 'layout/', description: 'Global layout of the application', children: [
              {name: 'footer/'},
              {name: 'navbar/'},
              {name: 'sidenav/'},
              {name: 'layout.module.ts', file: true}
            ]
          },
          {
            name: 'modules/', children: [
              {
                name: 'users/', children: [
                  {name: 'components/'},
                  {name: 'models/'},
                  {name: 'interfaces/'},
                  {name: 'enums/'},
                  {name: 'pipes/'},
                  {name: 'services/'},
                  {name: 'users.module.ts', file: true},
                  {name: 'users-routing.module.ts', file: true}
                ]
              },
            ]
          },
          {
            name: 'shared/', description: 'Common features useful for modules', children: [
              {
                name: 'components/', children: [
                  {name: 'perfect-card/'}
                ]
              },
              {name: 'constants/'},
              {
                name: 'enums/', children: [
                  {name: 'gender.enum.ts', file: true}
                ]
              },
              {name: 'interfaces/'},
              {
                name: 'models/', children: [
                  {name: 'user.model.ts', file: true}
                ]
              },
              {
                name: 'pipes/', children: [
                  {name: 'date.pipe.ts', file: true}
                ]
              },
              {
                name: 'services/', children: [
                  {name: 'user.service.ts', file: true}
                ]
              },
              {name: 'material.module.ts', description: 'Import/export all material Angular modules', file: true},
              {name: 'shared.module.ts', file: true}
            ]
          },
          {name: 'app.component.ts', file: true},
          {name: 'app.module.ts', file: true},
          {name: 'app-routing.module.ts', file: true}
        ]
      },
      {
        name: 'assets/', description: 'For all external files', children: [
          {
            name: 'i18n/', description: 'Internationalization files', children: [
              {name: 'en.ts', file: true},
              {name: 'fr.ts', file: true}
            ]
          },
          {
            name: 'icons/', children: [
              {name: 'english-flag.svg', file: true},
              {name: 'french-flag.svg', file: true}
            ]
          },
          {
            name: 'images/', children: [
              {name: 'logo.png', file: true}
            ]
          }
        ]
      },
      {name: 'environments/'},
      {
        name: 'styles/', description: 'All CSS files', children: [
          {name: 'specific-material.scss', file: true},
          {name: 'specific-html.scss', file: true},
          {name: 'usefull-class.scss', file: true},
          {name: 'themes.scss', file: true},
          {name: 'variables.scss', file: true}
        ]
      },
      {name: 'styles.scss', description: 'Import CSS files from styles folder', file: true},
      {name: 'index.html', file: true}
    ]
  }
];

@Component({
  selector: 'app-folders-organization',
  templateUrl: './folders-organization.component.html',
  styleUrls: ['./folders-organization.component.scss']
})
export class FoldersOrganizationComponent {

  treeControl = new NestedTreeControl<FolderNode>(node => node.children);
  dataSource = new MatTreeNestedDataSource<FolderNode>();

  constructor() {
    this.dataSource.data = TREE_DATA;
    this.treeControl.dataNodes = TREE_DATA;
  }

  hasChild = (_: number, node: FolderNode) => !!node.children && node.children.length > 0;

}
