import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FoldersOrganizationComponent } from './folders-organization.component';
import { CoreModule } from '@core/core.module';
import { SharedModule } from '@shared/shared.module';
import { RouterTestingModule } from '@angular/router/testing';

describe('FoldersOrganizationComponent', () => {
  let component: FoldersOrganizationComponent;
  let fixture: ComponentFixture<FoldersOrganizationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CoreModule, SharedModule, RouterTestingModule],
      declarations: [FoldersOrganizationComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FoldersOrganizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
