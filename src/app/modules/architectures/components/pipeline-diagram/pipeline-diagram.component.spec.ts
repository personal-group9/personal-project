import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PipelineDiagramComponent } from './pipeline-diagram.component';
import { CoreModule } from '@core/core.module';
import { SharedModule } from '@shared/shared.module';
import { RouterTestingModule } from '@angular/router/testing';

describe('PipelineDiagramComponent', () => {
  let component: PipelineDiagramComponent;
  let fixture: ComponentFixture<PipelineDiagramComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CoreModule, SharedModule, RouterTestingModule],
      declarations: [PipelineDiagramComponent]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PipelineDiagramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
