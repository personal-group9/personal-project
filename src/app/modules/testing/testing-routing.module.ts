import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UnitTestingComponent } from '@app/modules/testing/components/unit-testing/unit-testing.component';
import { EndToEndTestingComponent } from '@app/modules/testing/components/end-to-end-testing/end-to-end-testing.component';
import { NotFoundComponent } from '@app/modules/home/components/not-found/not-found.component';

const testingRoutes: Routes = [
  {
    path: 'unit-testing',
    component: UnitTestingComponent,
    data: {title: 'MODULES.TESTING.UNIT_TESTING.TITLE'}
  },
  {
    path: 'end-to-end-testing',
    component: EndToEndTestingComponent,
    data: {title: 'MODULES.TESTING.END_TO_END_TESTING.TITLE'}
  },
  {
    path: '**',
    component: NotFoundComponent,
    data: {title: 'MODULES.COMMON.NOT_FOUND.TITLE'}
  }
];

@NgModule({
  imports: [RouterModule.forChild(testingRoutes)],
  exports: [RouterModule]
})
export class TestingRoutingModule {
}
