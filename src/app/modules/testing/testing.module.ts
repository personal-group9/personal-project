import { NgModule } from '@angular/core';
import { UnitTestingComponent } from './components/unit-testing/unit-testing.component';
import { EndToEndTestingComponent } from './components/end-to-end-testing/end-to-end-testing.component';
import { TestingRoutingModule } from '@app/modules/testing/testing-routing.module';
import { SharedModule } from '@shared/shared.module';

@NgModule({
  declarations: [
    UnitTestingComponent,
    UnitTestingComponent,
    EndToEndTestingComponent
  ],
  imports: [
    SharedModule,
    TestingRoutingModule
  ]
})
export class TestingModule {
}
