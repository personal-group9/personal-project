import { Component } from '@angular/core';
import { SetupItem } from '@shared/interfaces/setup-item';
import { Syntax } from '@shared/enums/syntaxs.enum';

@Component({
  selector: 'app-unit-testing',
  templateUrl: './unit-testing.component.html',
  styleUrls: ['./unit-testing.component.scss']
})
export class UnitTestingComponent {

  setupStepList: SetupItem[] = [
    {
      title: 'COMMON.LABELS.USAGE', markdownList: [
        {title: 'Terminal', syntax: Syntax.BASH, content: 'npm run test'},
        {title: 'Terminal', syntax: Syntax.BASH, content: 'ng test'}
      ]
    },
  ];
}
