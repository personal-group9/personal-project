import { Component } from '@angular/core';
import { SetupItem } from '@shared/interfaces/setup-item';
import { Syntax } from '@shared/enums/syntaxs.enum';

@Component({
  selector: 'app-end-to-end-testing',
  templateUrl: './end-to-end-testing.component.html',
  styleUrls: ['./end-to-end-testing.component.scss']
})
export class EndToEndTestingComponent {

  setupStepList: SetupItem[] = [
    {
      title: 'COMMON.LABELS.USAGE', markdownList: [
        {title: 'Terminal', syntax: Syntax.BASH, content: 'npm run e2e'},
        {title: 'Terminal', syntax: Syntax.BASH, content: 'ng e2e'}
      ]
    },
  ];

}
