import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EndToEndTestingComponent } from './end-to-end-testing.component';
import { CoreModule } from '@core/core.module';
import { SharedModule } from '@shared/shared.module';
import { RouterTestingModule } from '@angular/router/testing';

describe('EndToEndTestingComponent', () => {
  let component: EndToEndTestingComponent;
  let fixture: ComponentFixture<EndToEndTestingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CoreModule, SharedModule, RouterTestingModule],
      declarations: [EndToEndTestingComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EndToEndTestingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
