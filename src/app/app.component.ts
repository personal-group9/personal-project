import { Component, HostBinding, HostListener } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { SvgIcon } from '@core/interfaces/svg-icon';
import { ThemeService } from '@core/services/theme.service';
import { OverlayContainer } from '@angular/cdk/overlay';
import { MatIconRegistry } from '@angular/material/icon';
import { ScrollbarService } from '@core/services/scrollbar.service';
import { Theme } from '@core/interfaces/theme';
import { BrowserTabTitleService } from '@core/services/browser-tab-title.service';
import { SVG_ICONS_LIST } from '@core/constants/svg-icon.constant';
import { THEMES_LIST } from '@core/constants/theme.constant';
import { SidenavService } from '@core/services/sidenav.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  @HostBinding('class') public cssClass!: string;

  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    private themeService: ThemeService,
    private overlayContainer: OverlayContainer,
    private browserTabTitleService: BrowserTabTitleService,
    private sidenavService: SidenavService,
    public scrollbarService: ScrollbarService,
  ) {
    this.addSvgIcons();
    this.updateTheme();
    this.browserTabTitleService.setBrowserTabTitle();
  }

  /**
   * Get windows size less the navbar height, to fix the scrollbar and the footer
   */
  @HostListener('window:resize')
  @HostListener('window:orientationchange')
  private onResize(): void {
    this.sidenavService.disableFixedMenuForMobile();
    this.scrollbarService.updateHeight();
  }

  private addSvgIcons(): void {
    SVG_ICONS_LIST.forEach((icon: SvgIcon) => {
      this.matIconRegistry.addSvgIcon(icon.name, this.domSanitizer.bypassSecurityTrustResourceUrl(icon.url));
    });
  }

  private updateTheme(): void {
    this.themeService.selectedTheme.subscribe((newTheme: Theme) => {
      this.cssClass = newTheme.value;
      this.overlayContainer.getContainerElement().classList.remove(...THEMES_LIST.map(theme => theme.value));
      this.overlayContainer.getContainerElement().classList.add(this.cssClass);
    });
  }

}
