import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticatedGuard } from '@core/authentication/guards/authenticated-guard.service';
import { NotFoundComponent } from '@app/modules/home/components/not-found/not-found.component';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./modules/home/home.module').then(m => m.HomeModule)
  },
  {
    path: 'management',
    canActivate: [AuthenticatedGuard],
    children: [
      {
        path: 'user',
        loadChildren: () => import('./modules/user/user.module').then(m => m.UserModule),
      },
      {
        path: '**',
        component: NotFoundComponent,
        data: {title: 'MODULES.COMMON.NOT_FOUND.TITLE'}
      }
    ]
  },
  {
    path: 'tutorial',
    children: [
      {
        path: 'architectures',
        loadChildren: () => import('./modules/architectures/architectures.module').then(m => m.ArchitecturesModule)
      },
      {
        path: 'devops',
        loadChildren: () => import('./modules/devops/devops.module').then(m => m.DevopsModule),
      },
      {
        path: 'code-analysis',
        loadChildren: () => import('./modules/code-analysis/code-analysis.module').then(m => m.CodeAnalysisModule),
      },
      {
        path: 'testing',
        loadChildren: () => import('./modules/testing/testing.module').then(m => m.TestingModule),
      },
      {
        path: 'styles',
        loadChildren: () => import('./modules/styles/styles.module').then(m => m.StylesModule),
      },
      {
        path: 'layout',
        loadChildren: () => import('./modules/layout/layout.module').then(m => m.LayoutModule),
      },
      {
        path: 'packages',
        loadChildren: () => import('./modules/packages/packages.module').then(m => m.PackagesModule),
      },
      {
        path: 'core',
        loadChildren: () => import('./modules/core/core.module').then(m => m.CoreModule),
      },
      {
        path: '**',
        component: NotFoundComponent,
        data: {title: 'MODULES.COMMON.NOT_FOUND.TITLE'}
      }
    ]
  },
  {
    path: '**',
    component: NotFoundComponent,
    data: {title: 'MODULES.COMMON.NOT_FOUND.TITLE'}
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
