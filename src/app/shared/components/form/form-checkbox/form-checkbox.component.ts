import { Component, Input } from '@angular/core';
import { coerceBooleanProperty } from '@angular/cdk/coercion';

@Component({
  selector: 'app-form-checkbox',
  templateUrl: './form-checkbox.component.html',
  styleUrls: ['./form-checkbox.component.scss']
})
export class FormCheckboxComponent {

  /**
   *  Mandatory parameter
   */
  @Input() model!: any;

  /**
   * Optional parameters
   */
  @Input() label!: string;
  @Input() set disabled(value: any) { this.disabledBoolean = coerceBooleanProperty(value); }
  @Input() set disabledRipple(value: any) { this.disabledRippleBoolean = coerceBooleanProperty(value); }

  disabledRippleBoolean!: boolean;
  disabledBoolean!: boolean;

}
