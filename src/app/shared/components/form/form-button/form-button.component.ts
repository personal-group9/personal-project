import { Component, Input } from '@angular/core';
import { ThemePalette } from '@angular/material/core';
import { coerceBooleanProperty } from '@angular/cdk/coercion';

@Component({
  selector: 'app-form-button',
  templateUrl: './form-button.component.html',
  styleUrls: ['./form-button.component.scss']
})
export class FormButtonComponent {

  @Input() label!: string;
  @Input() tooltip!: string;
  @Input() icon!: string;
  @Input() prefixIcon!: string;
  @Input() suffixIcon!: string;
  @Input() routerLink!: string;
  @Input() buttonClass!: string;
  @Input() buttonStyle!: string;
  /** 'button' | 'submit' | 'reset' */
  @Input() type!: string;
  /** 'primary' | 'secondary' | 'warn' */
  @Input() color!: ThemePalette;
  /** 'raised' | 'stroked' | 'flat' | 'icon' | 'fab' | 'mini-fab' */
  @Input() shape!: string;
  @Input() set disabled(value: any) { this.disabledBoolean = coerceBooleanProperty(value); }
  @Input() set disabledRipple(value: any) { this.disabledRippleBoolean = coerceBooleanProperty(value); }

  disabledRippleBoolean!: boolean;
  disabledBoolean!: boolean;

}
