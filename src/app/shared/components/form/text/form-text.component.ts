import { AfterViewInit, Component, ElementRef, Input, ViewChild } from '@angular/core';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { FloatLabelType, MatFormFieldAppearance } from '@angular/material/form-field';

@Component({
  selector: 'app-form-text',
  templateUrl: './form-text.component.html',
  styleUrls: ['./form-text.component.scss']
})
export class FormTextComponent implements AfterViewInit {

  /**
   *  Mandatory parameter
   */
  @Input() model!: any;

  /**
   * Optional parameters
   */
  @Input() label!: string;
  @Input() placeholder!: string;
  /** 'legacy' | 'standard' | 'fill' | 'outline' */
  @Input() appearance!: MatFormFieldAppearance;
  @Input() set required(value: any) { this.requiredBoolean = coerceBooleanProperty(value); }
  @Input() set disabled(value: any) { this.disabledBoolean = coerceBooleanProperty(value); }
  @Input() set focus(value: any) { this.focusBoolean = coerceBooleanProperty(value); }
  @Input() set clearButton(value: any) { this.clearButtonBoolean = coerceBooleanProperty(value); }
  @Input() prefixIcon!: string;
  @Input() suffixIcon!: string;
  @Input() hint!: string;
  @Input() minLength!: number;
  @Input() maxLength!: number;
  /** 'always' | 'never' | 'auto'; */
  @Input() floatLabelType!: FloatLabelType;

  @ViewChild('textInput') textInput!: ElementRef;
  requiredBoolean!: boolean;
  disabledBoolean!: boolean;
  focusBoolean!: boolean;
  clearButtonBoolean!: boolean;

  ngAfterViewInit(): void {
    if (this.focusBoolean && !this.disabledBoolean) { setTimeout(this.textInput.nativeElement.focus(), 100); }
  }
}
