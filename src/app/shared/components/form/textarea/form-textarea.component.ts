import { AfterViewInit, Component, ElementRef, Input, ViewChild } from '@angular/core';
import { FloatLabelType, MatFormFieldAppearance } from '@angular/material/form-field';
import { coerceBooleanProperty } from '@angular/cdk/coercion';

@Component({
  selector: 'app-form-textarea',
  templateUrl: './form-textarea.component.html',
  styleUrls: ['./form-textarea.component.scss']
})
export class FormTextareaComponent implements AfterViewInit {

  /**
   *  Mandatory parameter
   */
  @Input() model!: any;

  /**
   * Optional parameters
   */
  @Input() label!: string;
  @Input() placeholder!: string;
  /** 'legacy' | 'standard' | 'fill' | 'outline' */
  @Input() appearance!: MatFormFieldAppearance;
  @Input() set required(value: any) { this.requiredBoolean = coerceBooleanProperty(value); }
  @Input() set disabled(value: any) { this.disabledBoolean = coerceBooleanProperty(value); }
  @Input() set focus(value: any) { this.focusBoolean = coerceBooleanProperty(value); }
  @Input() hint!: string;
  @Input() minLength!: number;
  @Input() maxLength!: number;
  /** 'always' | 'never' | 'auto'; */
  @Input() floatLabelType!: FloatLabelType;
  @Input() rows = 3;

  @ViewChild('textInput') textInput!: ElementRef;
  requiredBoolean!: boolean;
  disabledBoolean!: boolean;
  focusBoolean!: boolean;

  ngAfterViewInit(): void {
    if (this.focusBoolean && !this.disabledBoolean) { setTimeout(this.textInput.nativeElement.focus(), 100); }
  }

}
