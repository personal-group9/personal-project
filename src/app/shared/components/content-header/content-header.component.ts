import { Component, Input, TemplateRef } from '@angular/core';
import { coerceBooleanProperty } from '@angular/cdk/coercion';

@Component({
  selector: 'app-content-header',
  templateUrl: './content-header.component.html',
  styleUrls: ['./content-header.component.scss']
})
export class ContentHeaderComponent {

  @Input() headerTitle!: string;
  @Input() subtitle!: string;
  @Input() iconTitle!: string;
  @Input() rightHeaderTemplate?: TemplateRef<any>;
  @Input() set backButton(value: any) { this.backButtonBoolean = coerceBooleanProperty(value); }

  backButtonBoolean!: boolean;

}
