import { Component, Input, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent {

  @Input() iconTitle?: string;
  @Input() cardTitle?: string;
  @Input() subtitle?: string;
  @Input() rightHeaderTemplate?: TemplateRef<any>;

}
