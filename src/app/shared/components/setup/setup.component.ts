import { Component, Input } from '@angular/core';
import { SetupItem } from '../../interfaces/setup-item';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-setup',
  templateUrl: './setup.component.html',
  styleUrls: ['./setup.component.scss']
})
export class SetupComponent {

  @Input() setupList: SetupItem[];

  constructor(
    private toastrService: ToastrService,
    private translateService: TranslateService
  ) {
    this.setupList = [];
  }

  public copyCode(): void {
    this.translateService.get('SHARED.SETUP.COPIED').subscribe((message: string) => {
      this.toastrService.info(message);
    });
  }

}
