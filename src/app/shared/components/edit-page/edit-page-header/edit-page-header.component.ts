import { Component, Input, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import ApiModel from '@core/api/api-model';

@Component({
  selector: 'app-edit-page-header',
  templateUrl: './edit-page-header.component.html',
  styleUrls: ['./edit-page-header.component.scss']
})
export class EditPageHeaderComponent implements OnInit {

  @Input() rightHeaderTemplate?: TemplateRef<any>;
  @Input() resourceName?: string;
  @Input() model!: ApiModel<any>;

  public newInput = true;
  public subtitle = '';

  constructor(private activatedRoute: ActivatedRoute) {

    this.activatedRoute.url.subscribe(fragment => {
      this.newInput = fragment[0].path === 'new';
    });
  }

  ngOnInit(): void {
    this.createSubtitle();
  }

  private createSubtitle(): void {
    if (this.model?.lastModifiedBy || this.model?.lastModifiedDate) {
      this.subtitle += 'Modifié';
      if (this.model?.createdBy){
        this.subtitle += ' par ' + this.model?.lastModifiedBy;
      }
      if (this.model?.createdDate){
        this.subtitle += ' le ' + this.model?.lastModifiedDate;
      }
    } else if (this.model?.createdBy || this.model?.createdDate) {
      this.subtitle += 'Crée';
      if (this.model?.createdBy){
        this.subtitle += ' par ' + this.model?.createdBy;
      }
      if (this.model?.createdDate){
        this.subtitle += ' le ' + this.model?.createdDate;
      }
    }
  }

}
