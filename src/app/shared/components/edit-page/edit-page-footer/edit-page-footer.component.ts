import { Component } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-edit-page-footer',
  templateUrl: './edit-page-footer.component.html',
  styleUrls: ['./edit-page-footer.component.scss']
})
export class EditPageFooterComponent {

  constructor(private location: Location) {}

  back(): void {
    this.location.back();
  }

}
