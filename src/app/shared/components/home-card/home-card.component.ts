import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-home-card',
  templateUrl: './home-card.component.html',
  styleUrls: ['./home-card.component.scss']
})
export class HomeCardComponent {

  @Input() cardTitle!: string;
  @Input() cardContent!: string;
  @Input() link!: string;
  @Input() disabled?: string;

}
