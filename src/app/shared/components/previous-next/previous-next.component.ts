import { Component, Input } from '@angular/core';
import { UNAUTHENTICATED_MENU } from '@core/constants/menu.constant';
import { MenuItem } from '@core/interfaces/menu-item';
import { Router } from '@angular/router';

@Component({
  selector: 'app-previous-next',
  templateUrl: './previous-next.component.html',
  styleUrls: ['./previous-next.component.scss']
})
export class PreviousNextComponent {

  @Input() previous?: { label: string, route: string };
  @Input() next?: { label: string, route: string };

  constructor(private router: Router) {
    this.getPreviousAndNext(UNAUTHENTICATED_MENU);
  }

  private getPreviousAndNext(menuItems: MenuItem[]): void {
    menuItems.forEach((item: MenuItem, index: number) => {
      if (item.children) {
        this.getPreviousAndNext(item.children);
      } else {
        if (this.router.url === item.route) {
          const next = menuItems[index + 1];
          const previous = menuItems[index - 1];
          if (!this.next && next && !next.disabled) {
            this.next = {label: next.title, route: next.route};
          }
          if (!this.previous && index !== 0 && !previous.disabled) {
            this.previous = {label: previous.title, route: previous.route};
          }
        }
      }
    });
  }

}
