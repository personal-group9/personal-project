import { Markdown } from '@shared/interfaces/markdown';

export interface SetupItem {
  title: string;
  description?: string;
  markdownList?: Markdown[];
}
