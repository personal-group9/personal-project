import { Syntax } from '@shared/enums/syntaxs.enum';

export interface Markdown {
  title: string;
  syntax?: Syntax;
  content: string;
}

