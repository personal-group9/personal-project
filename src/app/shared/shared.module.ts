import { NgModule } from '@angular/core';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '@shared/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { ContentHeaderComponent } from '@shared/components/content-header/content-header.component';
import { CardComponent } from '@shared/components/card/card.component';
import { MarkdownModule } from 'ngx-markdown';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { SetupComponent } from '@shared/components/setup/setup.component';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { HomeCardComponent } from '@shared/components/home-card/home-card.component';
import { RouterModule } from '@angular/router';
import { PreviousNextComponent } from '@shared/components/previous-next/previous-next.component';
import { AnavaiModule } from 'ngx-anavai';
import { EditPageHeaderComponent } from '@shared/components/edit-page/edit-page-header/edit-page-header.component';
import { EditPageFooterComponent } from '@shared/components/edit-page/edit-page-footer/edit-page-footer.component';
import { A11yModule } from '@angular/cdk/a11y';
import { FormTextComponent } from '@shared/components/form/text/form-text.component';
import { FormTextareaComponent } from '@shared/components/form/textarea/form-textarea.component';
import { FormSelectComponent } from '@shared/components/form/select/form-select.component';
import { FormCheckboxComponent } from '@shared/components/form/form-checkbox/form-checkbox.component';
import { FormRadioButtonComponent } from '@shared/components/form/form-radio-button/form-radio-button.component';
import { FormButtonComponent } from '@shared/components/form/form-button/form-button.component';
import { FormChipsComponent } from '@shared/components/form/form-chips/form-chips.component';
import { FormSlideToggleComponent } from '@shared/components/form/slide-toogle/form-slide-toggle.component';


@NgModule({
  declarations: [
    ContentHeaderComponent,
    CardComponent,
    SetupComponent,
    HomeCardComponent,
    PreviousNextComponent,
    EditPageHeaderComponent,
    EditPageFooterComponent,
    FormTextComponent,
    FormTextareaComponent,
    FormCheckboxComponent,
    FormSelectComponent,
    FormRadioButtonComponent,
    FormButtonComponent,
    FormChipsComponent,
    FormSlideToggleComponent
  ],
  imports: [
    PerfectScrollbarModule,
    MaterialModule,
    TranslateModule,
    ReactiveFormsModule,
    FormsModule,
    ToastrModule.forRoot(),
    MarkdownModule.forRoot(),
    ClipboardModule,
    RouterModule,
    AnavaiModule,
    A11yModule
  ],
  exports: [
    MaterialModule,
    PerfectScrollbarModule,
    MarkdownModule,
    TranslateModule,
    ReactiveFormsModule,
    FormsModule,
    ContentHeaderComponent,
    CardComponent,
    SetupComponent,
    ClipboardModule,
    HomeCardComponent,
    PreviousNextComponent,
    AnavaiModule,
    EditPageHeaderComponent,
    EditPageFooterComponent,
    FormTextComponent,
    FormTextareaComponent,
    FormCheckboxComponent,
    FormSelectComponent,
    FormRadioButtonComponent,
    FormButtonComponent,
    FormChipsComponent,
    FormSlideToggleComponent
  ],
  providers: [
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: {displayDefaultIndicatorType: false}
    }
  ]
})
export class SharedModule {
}
