export enum Syntax {
  BASH = 'bash',
  TYPESCRIPT = 'typescript',
  SCSS = 'scss',
  JSON = 'json',
  HTML = 'html',
  PROPERTIES = 'properties',
  JAVASCRIPT = 'javascript',
  YAML = 'yaml'
}
