import { Component } from '@angular/core';
import { AuthenticationService } from '@core/authentication/services/authentication.service';
import { ThemeService } from '@core/services/theme.service';
import { SidenavService } from '@core/services/sidenav.service';
import { Language } from '@core/interfaces/language';
import { Theme } from '@core/interfaces/theme';
import { InternationalizationService } from '@core/services/internationalization.service';
import { LANGUAGES_LIST } from '@core/constants/language.constant';
import { THEMES_LIST } from '@core/constants/theme.constant';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {

  languages: Language[] = LANGUAGES_LIST;
  themes: Theme[] = THEMES_LIST;

  constructor(
    public internationalizationService: InternationalizationService,
    public authenticationService: AuthenticationService,
    public themeService: ThemeService,
    public sidenavService: SidenavService
  ) {
  }

  changeTheme(theme: Theme): void {
    this.themeService.selectedTheme.next(theme);
  }

  changeLanguage(language: Language): void {
    this.internationalizationService.selectedLanguage.next(language);
  }

}
