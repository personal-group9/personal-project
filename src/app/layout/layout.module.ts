import { NgModule } from '@angular/core';
import { FooterComponent } from '@app/layout/footer/footer.component';
import { NavbarComponent } from '@app/layout/navbar/navbar.component';
import { SharedModule } from '@shared/shared.module';
import { RouterModule } from '@angular/router';
import { SidenavComponent } from './sidenav/sidenav.component';
import { SidenavItemComponent } from './sidenav-item/sidenav-item.component';

@NgModule({
  declarations: [
    NavbarComponent,
    FooterComponent,
    SidenavComponent,
    SidenavItemComponent
  ],
  imports: [
    SharedModule,
    RouterModule.forChild([])
  ],
  exports: [
    NavbarComponent,
    FooterComponent,
    SidenavComponent
  ]
})
export class LayoutModule {
}
