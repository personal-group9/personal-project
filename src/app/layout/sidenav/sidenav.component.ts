import { Component, OnInit, ViewChild } from '@angular/core';
import { SidenavService } from '@core/services/sidenav.service';
import { AuthenticationService } from '@core/authentication/services/authentication.service';
import { MenuItem } from '@core/interfaces/menu-item';
import { MatSidenav } from '@angular/material/sidenav';
import { AUTHENTICATED_MENU, MULTI_EXPANSION_PANELS_MENU, UNAUTHENTICATED_MENU, WIDTH_PX_MENU } from '@core/constants/menu.constant';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {

  @ViewChild('sidenav', {static: true}) private sidenav!: MatSidenav;
  public menu!: MenuItem[];
  public multiExpansionPanel = MULTI_EXPANSION_PANELS_MENU;
  public widthPxMenu = WIDTH_PX_MENU;

  constructor(
    public sidenavService: SidenavService,
    private authenticationService: AuthenticationService
  ) {
    this.selectMenu();
  }

  ngOnInit(): void {
    this.sidenavService.setSidenav(this.sidenav);
  }

  private selectMenu(): void {
    this.authenticationService.isLoggedInObservable().subscribe(loggedIn => {
      if (loggedIn) {
        this.menu = AUTHENTICATED_MENU;
      } else {
        this.menu = UNAUTHENTICATED_MENU;
      }
    });
  }

}
