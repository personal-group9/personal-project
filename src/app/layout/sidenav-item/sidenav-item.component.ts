import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem } from '@core/interfaces/menu-item';
import { SidenavService } from '@core/services/sidenav.service';
import { PADDING_LEFT_LEVEL_PX_MENU } from '@core/constants/menu.constant';

@Component({
  selector: 'app-sidenav-item',
  templateUrl: './sidenav-item.component.html',
  styleUrls: ['./sidenav-item.component.scss'],
})
export class SidenavItemComponent {

  @Input() item!: MenuItem;
  @Input() depth!: number;
  itemPaddingLeftPx = PADDING_LEFT_LEVEL_PX_MENU;

  constructor(
    public router: Router,
    public sidenavService: SidenavService
  ) {
  }

}
