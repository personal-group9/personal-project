import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CoreModule } from '@core/core.module';
import { AppComponent } from '@app/app.component';
import { AppRoutingModule } from '@app/app-routing.module';
import { LayoutModule } from '@app/layout/layout.module';
import { SharedModule } from '@shared/shared.module';

/**
 * Fix Heroku
 */

/**
 * DEVELOP
 * - table + database
 * - Logger
 * - security analysis
 */

/**
 * $ cd ./dist/my-lib
 * $ npm link
 *
 * $ cd my-app
 * $ npm link my-lib
 */


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CoreModule,
    SharedModule,
    LayoutModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
