// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyDrrE1wOZWW3gc5GTQqLFDlV1NlVG3Gxw4',
    authDomain: 'personal-project-b2819.firebaseapp.com',
    databaseURL: 'https://personal-project-b2819.firebaseio.com',
    projectId: 'personal-project-b2819',
    storageBucket: 'personal-project-b2819.appspot.com',
    messagingSenderId: '339981312508',
    appId: '1:339981312508:web:85a805d54053b5b6ef5279',
    measurementId: 'G-2RQ49EYK8Q'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
