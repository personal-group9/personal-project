# Personal Project

Personal website project containing several tutorials for developing an Angular application with maintainable and scalable code.

## Hosting servers

Two hosts for this project:
- [Firebase](https://personal-project-b2819.web.app/)
- [Heroku](https://shrouded-savannah-47750.herokuapp.com/)

## Code quality

You can access to the [SonarCloud report](https://sonarcloud.io/dashboard?id=personal-group9_personal-project).

## Binary repository manager

You can access to different versions of the binary project on [Artifactory](https://personalproject.jfrog.io/artifactory/personal-project/personal-project-1.0.0.tgz).

## Environment

This project was generated with 
- [Angular CLI](https://github.com/angular/angular-cli) version  `12.0.4`
- [Npm](https://www.npmjs.com/) version `6.4.13`
- [Node.js](https://nodejs.org/) version `14.17.0`

## Software architecture

![](src/assets/images/software-architecture.png)

## Pipeline steps

![](src/assets/images/pipeline-diagram.png)

## Set up

Run `npm install` to install project dependencies.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
